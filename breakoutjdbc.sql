-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Machine: localhost
-- Genereertijd: 18 dec 2014 om 09:58
-- Serverversie: 5.6.12-log
-- PHP-versie: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databank: `breakoutjdbc`
--
CREATE DATABASE IF NOT EXISTS `breakoutjdbc` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `breakoutjdbc`;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `bricks`
--

CREATE TABLE IF NOT EXISTS `bricks` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `color` varchar(10) NOT NULL,
  `bwidthid` int(11) unsigned NOT NULL,
  `score` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_bricks` (`bwidthid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Gegevens worden uitgevoerd voor tabel `bricks`
--

INSERT INTO `bricks` (`id`, `name`, `color`, `bwidthid`, `score`) VALUES
(1, 'green1', 'GREEN', 1, 60),
(2, 'green2', 'GREEN', 2, 50),
(3, 'green3', 'GREEN', 3, 40),
(4, 'green4', 'GREEN', 4, 30),
(5, 'green5', 'GREEN', 5, 20),
(6, 'yellow1', 'YELLOW', 1, 70),
(7, 'yellow2', 'YELLOW', 2, 60),
(8, 'yellow3', 'YELLOW', 3, 50),
(9, 'yellow4', 'YELLOW', 4, 40),
(10, 'yellow5', 'YELLOW', 5, 30),
(11, 'blue1', 'BLUE', 1, 80),
(12, 'blue2', 'BLUE', 2, 70),
(13, 'blue3', 'BLUE', 3, 60),
(14, 'blue4', 'BLUE', 4, 50),
(15, 'blue5', 'BLUE', 5, 40),
(16, 'orange1', 'ORANGE', 1, 90),
(17, 'orange2', 'ORANGE', 2, 80),
(18, 'orange3', 'ORANGE', 3, 70),
(19, 'orange4', 'ORANGE', 4, 60),
(20, 'orange5', 'ORANGE', 5, 50),
(21, 'red1', 'RED', 1, 100),
(22, 'red2', 'RED', 2, 90),
(23, 'red3', 'RED', 3, 80),
(24, 'red4', 'RED', 4, 70),
(25, 'red5', 'RED', 5, 60);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `brickwidth`
--

CREATE TABLE IF NOT EXISTS `brickwidth` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `width` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Gegevens worden uitgevoerd voor tabel `brickwidth`
--

INSERT INTO `brickwidth` (`id`, `width`) VALUES
(1, 40),
(2, 60),
(3, 80),
(4, 100),
(5, 120);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `difficulties`
--

CREATE TABLE IF NOT EXISTS `difficulties` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `dname` varchar(50) NOT NULL,
  `psize` varchar(50) NOT NULL,
  `bspeed` int(11) unsigned NOT NULL,
  `lives` int(11) unsigned NOT NULL,
  `powerfreq` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Gegevens worden uitgevoerd voor tabel `difficulties`
--

INSERT INTO `difficulties` (`id`, `dname`, `psize`, `bspeed`, `lives`, `powerfreq`) VALUES
(1, 'easy', 'normalPaddle', 2, 5, 5),
(2, 'medium', 'paddleSizeMinusOne', 3, 3, 7),
(3, 'hard', 'paddleSizeMinusOne', 4, 1, 10);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `hsmp`
--

CREATE TABLE IF NOT EXISTS `hsmp` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `p1name` varchar(50) NOT NULL,
  `p2name` varchar(50) NOT NULL,
  `p1score` int(11) unsigned NOT NULL,
  `p2score` int(11) unsigned NOT NULL,
  `totalScore` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Gegevens worden uitgevoerd voor tabel `hsmp`
--

INSERT INTO `hsmp` (`id`, `p1name`, `p2name`, `p1score`, `p2score`, `totalScore`) VALUES
(4, 'Arry', 'Siebeert', 1600, 2410, 4010),
(5, 'ArryOne', 'ArryTwo', 6560, 2540, 9100),
(8, 'Arry', 'ThoSie', 2910, 1390, 4300),
(9, 'Thomasson177', 'Arry2000', 3405, 2910, 6315),
(10, 'Tom', 'Arne', 2130, 2980, 5110);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `hssp`
--

CREATE TABLE IF NOT EXISTS `hssp` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `p1name` varchar(50) NOT NULL,
  `p1score` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=88 ;

--
-- Gegevens worden uitgevoerd voor tabel `hssp`
--

INSERT INTO `hssp` (`id`, `p1name`, `p1score`) VALUES
(2, 'Arry', 2510),
(3, 'Arry', 3170),
(53, 'Arry', 2160),
(57, 'Arry', 7360),
(58, 'Arry', 7360),
(59, 'Arry2000', 6380),
(61, 'Arry', 2810),
(64, 'Arry', 2950),
(65, 'Arry', 1890),
(70, 'Arry', 5010),
(71, 'Tommy', 160),
(74, 'Arry', 3380),
(75, 'Arry2000', 3280),
(77, 'Arry2000', 11640),
(83, 'Arry2000', 33960),
(84, 'Arry2000', 48945),
(85, 'ArryTopScore', 67260);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `levels`
--

CREATE TABLE IF NOT EXISTS `levels` (
  `id` int(11) unsigned NOT NULL,
  `nrofrows` int(11) unsigned NOT NULL,
  `nrofrowsMP` int(11) unsigned NOT NULL,
  `bwidthid` int(11) unsigned NOT NULL,
  `pufreq` int(11) unsigned NOT NULL,
  `pdfreq` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `bwidthid` (`bwidthid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden uitgevoerd voor tabel `levels`
--

INSERT INTO `levels` (`id`, `nrofrows`, `nrofrowsMP`, `bwidthid`, `pufreq`, `pdfreq`) VALUES
(1, 2, 2, 5, 2, 10000),
(2, 2, 3, 5, 3, 15),
(3, 2, 3, 4, 4, 15),
(4, 3, 4, 4, 5, 10),
(5, 3, 4, 3, 6, 8),
(6, 3, 5, 3, 7, 6),
(7, 4, 5, 2, 10, 5),
(8, 4, 6, 2, 15, 4),
(9, 4, 6, 1, 20, 3),
(10, 5, 7, 1, 10000, 2);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `powers`
--

CREATE TABLE IF NOT EXISTS `powers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `url` varchar(50) NOT NULL,
  `description` varchar(100) NOT NULL,
  `type` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Gegevens worden uitgevoerd voor tabel `powers`
--

INSERT INTO `powers` (`id`, `name`, `url`, `description`, `type`) VALUES
(1, 'megaBall', 'images/megaBallPowerUp', 'Increases your ball size', 'up'),
(2, 'PSIncrease', 'images/PaddleSizeIncrease', 'Increases your paddle size', 'up'),
(3, 'slowBall', 'images/slowBall', 'Decreases ball speed', 'up'),
(4, 'extraLife', 'images/extraLife', 'Gives you an extra life', 'up'),
(6, 'miniBall', 'images/miniBallPowerDown', 'Decreases your ball size', 'down'),
(7, 'PSDecrease', 'images/PaddleSizeDecrease', 'Decreases your paddle size', 'down'),
(8, 'fastBall', 'images/fastBall', 'Increases ball speed', 'down'),
(9, 'reverseControls', 'images/reverseControls', 'Temporarily reverses your paddle''s controls', 'down'),
(10, 'gain200', 'images/gain200', 'Gain 200 points', 'up'),
(11, 'lose200', 'images/lose200', 'Lose 200 points', 'down');

--
-- Beperkingen voor gedumpte tabellen
--

--
-- Beperkingen voor tabel `bricks`
--
ALTER TABLE `bricks`
  ADD CONSTRAINT `FK_bricks` FOREIGN KEY (`bwidthid`) REFERENCES `brickwidth` (`id`);

--
-- Beperkingen voor tabel `levels`
--
ALTER TABLE `levels`
  ADD CONSTRAINT `FK_levels` FOREIGN KEY (`bwidthid`) REFERENCES `brickwidth` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
