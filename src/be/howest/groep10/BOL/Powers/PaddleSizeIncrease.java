package be.howest.groep10.BOL.Powers;

import be.howest.groep10.BOL.Game.GameBoard;
import be.howest.groep10.BOL.Game.Paddle;

import java.util.List;

/**
 * Created by Thomas on 19/11/2014.
 */
public class PaddleSizeIncrease extends Power {

    private List<Paddle> paddles;

    public PaddleSizeIncrease(int x, int y, GameBoard gameBoard) throws Exception {
        super(x, y);
        setImage(breakoutDAO.getPowerUrl("PSIncrease")+".png");
        this.paddles = gameBoard.getPaddles();
    }

    @Override
    public void activatePowerForPlayer1() {
        for(Paddle p :paddles)
        {
            p.PaddleSizeIncrease();
        }
    }

    @Override
    public void activatePowerForPlayer2() {
        for(Paddle p :paddles)
        {
            p.PaddleSizeIncrease();
        }
    }
}
