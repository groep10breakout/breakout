package be.howest.groep10.BOL.Powers;

import be.howest.groep10.BOL.Game.GameBoard;
import be.howest.groep10.BOL.Game.Score;

/**
 * Created by Arry on 19/11/2014.
 */
public class GainScore extends Power {

    private Score score;

    public GainScore(int x, int y, GameBoard gameBoard) throws Exception {
        super(x, y);
        setImage(breakoutDAO.getPowerUrl("gain200")+".png");
        this.score = gameBoard.getScore();
    }

    @Override
    public void activatePowerForPlayer1() {
        score.addScore1(200);
    }

    @Override
    public void activatePowerForPlayer2() {
        score.addScore2(200);
    }
}
