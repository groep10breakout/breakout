package be.howest.groep10.BOL.Powers;

import be.howest.groep10.BOL.Game.Ball;
import be.howest.groep10.BOL.Game.GameBoard;

import java.util.List;

/**
 * Created by Thomas on 19/11/2014.
 */
public class MiniBall extends  Power {

    private List<Ball> ballList;


    public MiniBall(int x, int y, GameBoard  gameBoard) throws Exception {
        super(x, y);
        setImage(breakoutDAO.getPowerUrl("miniBall")+".png");
        this.ballList = gameBoard.getBallList();
    }

    @Override
    public void activatePowerForPlayer1() {
        for(Ball b : ballList) {
            b.setMiniBall();
        }
    }

    @Override
    public void activatePowerForPlayer2() {
        for(Ball b : ballList) {
            b.setMiniBall();
        }
    }
}
