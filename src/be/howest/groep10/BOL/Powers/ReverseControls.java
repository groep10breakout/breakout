package be.howest.groep10.BOL.Powers;

import be.howest.groep10.BOL.Game.GameBoard;
import be.howest.groep10.BOL.Game.Score;

/**
 * Created by Arry on 1/12/2014.
 * \m/
 */
public class ReverseControls extends Power {

    private GameBoard gameBoard;
    private Score score;

    public ReverseControls(int x, int y, GameBoard gameBoard) throws Exception {
        super(x, y);
        setImage(breakoutDAO.getPowerUrl("reverseControls")+".png");
        this.gameBoard = gameBoard;
        score = gameBoard.getScore();
    }

    @Override
    public void activatePowerForPlayer1() {
        gameBoard.reverseControls();
    }

    @Override
    public void activatePowerForPlayer2() {
        gameBoard.reverseControls();
    }
}
