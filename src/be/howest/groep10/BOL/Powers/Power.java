package be.howest.groep10.BOL.Powers;

import be.howest.groep10.DAL.BreakoutDAO;
import be.howest.groep10.BOL.Game.Launcher;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by Thomas on 19/11/2014.
 */
public abstract class Power {
    protected int x, y;
    protected BufferedImage img;
    protected BreakoutDAO breakoutDAO;

    public Power(int x, int y) {
        setCoords(x, y);
        try {
            breakoutDAO = Launcher.dao;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void setImage(String path) {
        try {

            img = ImageIO.read(new File(path));
        }
        catch (IOException ie) {
            System.out.println("Power image not loaded correctly");
        }
    }

    private void draw(Graphics g) {
        g.drawImage(img, x, y, null);
    }

    public abstract void activatePowerForPlayer1();

    public abstract void activatePowerForPlayer2();

    public void fallDown(Graphics g) {
        draw(g);
        y += 2;
    }

    public void fallUp(Graphics g) {
        draw(g);
        y -= 2;
    }

    public Rectangle2D.Double getVirtualPower() {
        return new Rectangle2D.Double(x, y, 30 , 30);
    }

    public void remove() {
        setCoords(-30, -30);
        img = null;
    }

    public void setCoords(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
