package be.howest.groep10.BOL.Powers;

import be.howest.groep10.BOL.Game.GameBoard;
import be.howest.groep10.BOL.Game.Score;

/**
 * Created by Thomas on 19/11/2014.
 */
public class ExtraLife extends Power {

    private Score score;

    public ExtraLife(int x, int y, GameBoard gameBoard) throws Exception {
        super(x, y);
        setImage(breakoutDAO.getPowerUrl("extraLife")+".png");
        this.score = gameBoard.getScore();
    }

    @Override
    public void activatePowerForPlayer1() {
        score.extraLife();
    }

    @Override
    public void activatePowerForPlayer2() {
        score.extraLife();
    }
}
