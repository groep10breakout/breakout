package be.howest.groep10.BOL.Powers;

/**
 * Created by Arry on 21/11/2014.
 */
public class NullPower extends Power {

    public NullPower(int x, int y) {
        super(x, y);
    }

    @Override
    public void activatePowerForPlayer1() {

    }

    @Override
    public void activatePowerForPlayer2() {

    }


}
