package be.howest.groep10.BOL.PaddleStatePattern;

import be.howest.groep10.BOL.Game.Paddle;

/**
 * Created by Arry on 27/11/2014.
 */
public class PaddleSizePlusOneState implements PaddleSizeState {

    private Paddle paddle;

    public PaddleSizePlusOneState(Paddle paddle) {
        this.paddle = paddle;
        paddle.setWidth(230);
        if (paddle.isPlayer2()) {
            paddle.setImagePlayer2("images/paddleSizePlusOne2.png");
        } else {
            paddle.setImagePlayer1("images/paddleSizePlusOne.png");
        }
    }

    @Override
    public void increaseSize() {
        paddle.setState(new PaddleSizePlusTwoState(paddle));
    }

    @Override
    public void decreaseSize() {
        paddle.setState(new PaddleSizeNormalState(paddle));
    }
}
