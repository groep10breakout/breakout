package be.howest.groep10.BOL.PaddleStatePattern;

import be.howest.groep10.BOL.Game.Paddle;

/**
 * Created by Arry on 27/11/2014.
 */
public class PaddleSizeNormalState implements PaddleSizeState  {

    private Paddle paddle;

    public PaddleSizeNormalState(Paddle paddle)
    {
        this.paddle = paddle;
        paddle.setWidth(170);
        if (paddle.isPlayer2()) {
            paddle.setImagePlayer2("images/normalPaddle2.png");
        } else {
            paddle.setImagePlayer1("images/normalPaddle.png");
        }
    }

    @Override
    public void increaseSize() {
        paddle.setState(new PaddleSizePlusOneState(paddle));
    }

    @Override
    public void decreaseSize() {
        paddle.setState(new PaddleSizeMinusOneState(paddle));
    }

}
