package be.howest.groep10.BOL.PaddleStatePattern;

/**
 * Created by Arry on 27/11/2014.
 */
public interface PaddleSizeState {

    public void increaseSize();
    public void decreaseSize();
}
