package be.howest.groep10.BOL.PaddleStatePattern;

import be.howest.groep10.BOL.Game.Paddle;

/**
 * Created by Arry on 27/11/2014.
 */
public class PaddleSizePlusTwoState implements PaddleSizeState {

    private Paddle paddle;

    public PaddleSizePlusTwoState(Paddle paddle) {
        this.paddle = paddle;
        paddle.setWidth(300);
        if (paddle.isPlayer2()) {
            paddle.setImagePlayer2("images/paddleSizePlusTwo2.png");
        } else {
            paddle.setImagePlayer1("images/paddleSizePlusTwo.png");
        }
    }

    @Override
    public void increaseSize() {
        //no more increasing
    }

    @Override
    public void decreaseSize() {
        paddle.setState(new PaddleSizePlusOneState(paddle));
    }
}
