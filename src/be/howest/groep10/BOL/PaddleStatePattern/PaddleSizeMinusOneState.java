package be.howest.groep10.BOL.PaddleStatePattern;

import be.howest.groep10.BOL.Game.Paddle;

/**
 * Created by Arry on 27/11/2014.
 */
public class PaddleSizeMinusOneState implements PaddleSizeState{

    private Paddle paddle;

    public PaddleSizeMinusOneState(Paddle paddle)
    {
        this.paddle = paddle;
        paddle.setWidth(120);
        if (paddle.isPlayer2()) {
            paddle.setImagePlayer2("images/paddleSizeMinusOne2.png");
        } else {
            paddle.setImagePlayer1("images/paddleSizeMinusOne.png");
        }
    }

    @Override
    public void increaseSize() {
        paddle.setState(new PaddleSizeNormalState(paddle));
    }

    @Override
    public void decreaseSize() {
        paddle.setState(new PaddleSizeMinusTwoState(paddle));
    }
}
