package be.howest.groep10.BOL.PaddleStatePattern;

import be.howest.groep10.BOL.Game.Paddle;

/**
 * Created by Arry on 27/11/2014.
 */
public class PaddleSizeMinusTwoState implements PaddleSizeState {

    private Paddle paddle;

    public PaddleSizeMinusTwoState(Paddle paddle) {
        this.paddle = paddle;
        paddle.setWidth(80);
        if (paddle.isPlayer2()) {
            paddle.setImagePlayer2("images/paddleSizeMinusTwo2.png");
        } else {
            paddle.setImagePlayer1("images/paddleSizeMinusTwo.png");
        }
    }

    @Override
    public void increaseSize() {
        paddle.setState(new PaddleSizeMinusOneState(paddle));
    }

    @Override
    public void decreaseSize() {
        //no more decreasing of paddleSize
    }
}
