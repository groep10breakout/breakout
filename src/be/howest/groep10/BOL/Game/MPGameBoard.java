package be.howest.groep10.BOL.Game;

import be.howest.groep10.BOL.Powers.*;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Arry on 16/11/2014.
 */
public class MPGameBoard extends GameBoard {

    private Paddle paddle1;
    private Paddle paddle2;
    private Ball ball1;
    private Ball ball2;
    private List<Power> powersToPlayer1;
    private List<Power> powersToPlayer2;

    public MPGameBoard(FrmMain frmMain, Difficulty difficulty) {
        super(frmMain, difficulty);
    }

    @Override
    protected void createAdditionalComponents() {
        paddle1 = new Paddle(this, diff, false);
        paddle2 = new Paddle(this, diff, true);
        paddleList.add(paddle1);
        paddleList.add(paddle2);
        ball1 = new Ball(this, diff, paddle1);
        ball2 = new Ball(this, diff, paddle2);
        addToBallList(ball1);
        addToBallList(ball2);
        powersToPlayer1 = new ArrayList<Power>();
        powersToPlayer2 = new ArrayList<Power>();
    }

    @Override
    public boolean isMultiplayer() {
        return true;
    }

    @Override
    protected void drawPowers(Graphics g) {
        for(Power p : powersToPlayer1)
        {
            p.fallDown(g);
        }
        for(Power p : powersToPlayer2)
        {
            p.fallUp(g);
        }
    }

    @Override
    protected void drawPause(Graphics g) {
        if(!play)
        {
            g.setFont(new Font("Impact", Font.PLAIN, GameResizer.resizeInt(35)));
            g.setColor(Color.WHITE);
            g.drawString("Game Paused: hit space to continue",GameResizer.resizeInt(250),GameResizer.resizeInt(600));
            g.drawString("Game Paused: hit space to continue",GameResizer.resizeInt(250),GameResizer.resizeInt(100));
        }
    }

    @Override
    protected void catchPowers() {
        for(Power p : powersToPlayer1)
        {
            paddle1.catchPower(p);
        }
        for(Power p: powersToPlayer2)
        {
            paddle2.catchPower(p);
        }
    }

    @Override
    protected void ballActions() {
        if(ballList.size() > 0) {
            for (Ball b : ballList) {
                b.move();
                b.checkPaddleCollision(paddle1);
                b.checkPaddleCollision(paddle2);
                ballBrickInteraction(b);
            }
        }
    }

    @Override
    protected void ballBrickInteraction(Ball b)
    {
        Rectangle2D.Double bal = b.getVirtualBall();
        ArrayList<Brick> toBeRemoved = new ArrayList<Brick>();
        for(Brick brick : bricks)
        {
            Rectangle2D.Double br = brick.getVirtualBrick();
            if(br.intersects(bal))
            {
                toBeRemoved.add(brick);
                if (b.getOwner().isPlayer2()) {
                    powersToPlayer2.add(brick.getPower());
                } else {
                    powersToPlayer1.add(brick.getPower());
                }
            }
        }
        bounceBall(b, toBeRemoved);
        for(Brick br : toBeRemoved)
        {
            if (b.getOwner().isPlayer2()) {
                score.addScore2(br.getScore());
            } else {
                score.addScore1(br.getScore());
            }
            br.destroy();
            bricks.remove(br);
        }
    }

    @Override
    protected void fillNewBrickList() {
        bricks = levelBuilder.fillBrickListMP();
    }

    @Override
    protected void checkArrowKeysPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            if (reversedControls) {
                paddle1.goingRight = true;
            } else {
                paddle1.goingLeft = true;
            }
        }
        if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            if (reversedControls) {
                paddle1.goingLeft = true;
            } else {
                paddle1.goingRight = true;
            }
        }
    }

    @Override
    protected void checkArrowKeysReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            if (reversedControls) {
                paddle1.goingRight = false;
            } else {
                paddle1.goingLeft = false;
            }
        }
        if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            if (reversedControls) {
                paddle1.goingLeft = false;
            } else {
                paddle1.goingRight = false;
            }
        }
    }

    @Override
    protected void checkQorDPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_Q) {
            if (reversedControls) {
                paddle2.goingRight = true;
            } else {
                paddle2.goingLeft = true;
            }
        }
        if (e.getKeyCode() == KeyEvent.VK_D) {
            if (reversedControls) {
                paddle2.goingLeft = true;
            } else {
                paddle2.goingRight = true;
            }
        }
    }

    @Override
    protected void checkQorDReleased(KeyEvent e) {
        if  (e.getKeyCode() == KeyEvent.VK_Q) {
            if (reversedControls) {
                paddle2.goingRight = false;
            } else {
                paddle2.goingLeft = false;
            }
        }
        if  (e.getKeyCode() == KeyEvent.VK_D) {
            if (reversedControls) {
                paddle2.goingLeft = false;
            } else {
                paddle2.goingRight = false;
            }
        }
    }

    @Override
    protected void removePowers() {
        for(Power p : powersToPlayer1) {
            p.remove();
        }
        for(Power p : powersToPlayer2)
        {
            p.remove();
        }
    }

    @Override
    protected void resetBallProperties() {
        ball1.setOwner(paddle1);
        ball2.setOwner(paddle2);
        if(!ballListContainsBall(ball1))
            addToBallList(ball1);
        if(!ballListContainsBall(ball2))
            addToBallList(ball2);
    }

    @Override
    public void loseBall(Ball ball) {
        stopGame();
        removeFromBallList(ball);
        if(diff.getDifficulty() == Difficulty.EASY || diff.getDifficulty() == Difficulty.MEDIUM) {
            if (ballList.size() <= 0) {
                loseLife();
            }
        }else {
            if(ballList.size() < 2)
                loseLife();
        }
        startGame();
    }
}
