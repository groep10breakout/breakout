package be.howest.groep10.BOL.Game;

import be.howest.groep10.BOL.Powers.*;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Arry on 27/11/2014.
 */
public class BrickFactory {

    private List<Brick> bricks;
    private GameBoard board;

    public BrickFactory(GameBoard board)
    {
        bricks = new ArrayList<Brick>();
        this.board = board;
    }

    public List<Brick> getBrickList(int puFreq, int pdFreq)
    {
        for(Brick b : bricks)
        {
            addRandomPowerDown(b, pdFreq);
            addRandomPowerUp(b, puFreq);
        }
        return bricks;
    }

    public void addRowWithColor(int y, int brickWidth,  Color color, int score)
    {
        int xPos = GameResizer.resizeInt(20);
        if(board.getWidth() % brickWidth == 0)
                xPos = 0;
        int i = bricks.size();
        do {
            bricks.add(new Brick(xPos, y, brickWidth ,color, score));
            xPos += bricks.get(i).getWidth();
            i++;
        }
        while(xPos + bricks.get(i-1).getWidth() <= board.getWidth());
    }

    private void addRandomPowerUp(Brick brick, int freq)
    {
        Random rnd = new Random();
        if(rnd.nextInt(freq) == 0) {
            Power power = chooseRandomPowerUp();
            brick.addPower(power);
            power.setCoords(brick.getX(), brick.getY());
        }
    }

    private void addRandomPowerDown(Brick brick, int freq)
    {
        Random rnd = new Random();
        if(rnd.nextInt(freq) == 0) {
            Power power = chooseRandomPowerDown();
            brick.addPower(power);
            power.setCoords(brick.getX(), brick.getY());
        }
    }

    private Power chooseRandomPowerUp() {
        try {
            Random rnd = new Random();
            switch (rnd.nextInt(5)) {
                case 0: return new MegaBall(0, 0, board);
                case 1: return new ExtraLife(0, 0, board);
                case 2: return new GainScore(0, 0, board);
                case 3: return new PaddleSizeIncrease(0, 0, board);
                case 4: return new SlowBall(0, 0, board);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new NullPower(0, 0);
    }

    private Power chooseRandomPowerDown() {
        try {
            Random rnd = new Random();
            switch (rnd.nextInt(5))
            {
                case 0: return new FastBall(0,0, board);
                case 1: return new LoseScore(0,0, board);
                case 2: return new MiniBall(0,0, board);
                case 3: return new PaddleSizeDecrease(0,0, board);
                case 4: return new ReverseControls(0,0, board);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new NullPower(0,0);
    }
}
