package be.howest.groep10.BOL.Game;

import be.howest.groep10.DAL.BreakoutDAO;

import java.awt.*;
import java.util.List;

/**
 * Created by Siebe on 15/12/2014.
 */
public class LevelBuilder {
    private int level;
    private Score score;
    private int nrOfRows;
    private int bWidth;
    private int puFreq;
    private int pdFreq;

    private BrickFactory brickFactory;
    private BreakoutDAO dao;

    public LevelBuilder(GameBoard board, Score score) {
        this.score = score;
        brickFactory = new BrickFactory(board);
    }

    public List<Brick> fillBrickList() {
        setLevelSettings(false);
        brickFactory.addRowWithColor(GameResizer.resizeInt(290), bWidth, Color.GREEN, setbScore("GREEN", bWidth));
        brickFactory.addRowWithColor(GameResizer.resizeInt(255), bWidth, Color.YELLOW, setbScore("YELLOW", bWidth));
        if (nrOfRows >= 3)
            brickFactory.addRowWithColor(GameResizer.resizeInt(220), bWidth, Color.BLUE, setbScore("BLUE", bWidth));
        if (nrOfRows >= 4)
            brickFactory.addRowWithColor(GameResizer.resizeInt(185), bWidth, Color.ORANGE, setbScore("ORANGE", bWidth));
        if (nrOfRows >= 5)
            brickFactory.addRowWithColor(GameResizer.resizeInt(150), bWidth, Color.RED, setbScore("RED", bWidth));
        //here is where we would need to add new if's, if we had more level/rows
        return brickFactory.getBrickList(puFreq, pdFreq);
    }

    public List<Brick> fillBrickListMP() {
        setLevelSettings(true);
        int y1, y2;
        int addRowHeigth = Brick.getHeigth();
        System.out.println(addRowHeigth);
        if (nrOfRows % 2 == 0 ) {
            y1 = y2 = GameResizer.resizeInt(360);
        }
        else {
            y1 = y2 = GameResizer.resizeInt(345);
        }
        //here is where we would need to add new if's, if we had more level/rows
        if(nrOfRows >= 7 ) {
            brickFactory.addRowWithColor(y1, bWidth, Color.RED, setbScore("RED",bWidth));
            y1 += addRowHeigth;
        }
        if(nrOfRows >= 6) {
            brickFactory.addRowWithColor(y1, bWidth, Color.ORANGE, setbScore("ORANGE",bWidth));
            y2 -= addRowHeigth;
        }
        if(nrOfRows >= 5) {
            brickFactory.addRowWithColor(y2, bWidth, Color.ORANGE, setbScore("ORANGE",bWidth));
            y1 += addRowHeigth;
        }
        if(nrOfRows >= 4) {
            brickFactory.addRowWithColor(y1, bWidth, Color.YELLOW, setbScore("YELLOW",bWidth));
            y2 -= addRowHeigth;
        }
        if(nrOfRows >= 3) {
            brickFactory.addRowWithColor(y2, bWidth, Color.YELLOW, setbScore("YELLOW",bWidth));
            y1 += addRowHeigth;
        }
        brickFactory.addRowWithColor(y1, bWidth, Color.GREEN, setbScore("GREEN",bWidth));
        y2 -= addRowHeigth;
        brickFactory.addRowWithColor(y2, bWidth, Color.GREEN, setbScore("GREEN",bWidth));
        return brickFactory.getBrickList(puFreq, pdFreq);
    }

    private void setLevelSettings(boolean multiplayer) {
        setLevel();
        try {
            dao = Launcher.dao;
            setNrOfRows(multiplayer);
            setbWidth();
            setPuFreq();
            setPdFreq();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setLevel() {
        this.level = score.getLevel();
    }

    public void setNrOfRows(boolean multiplayer) throws Exception {
        nrOfRows = dao.getNrOfRows(level, multiplayer);
    }

    public void setbWidth() throws Exception {
        bWidth = dao.getBrickWidth(level);
    }

    public void setPuFreq() throws Exception {
        puFreq = dao.getPowerUpFrequency(level);
    }

    public void setPdFreq() throws Exception {
        pdFreq = dao.getPowerDownFrequency(level);
    }

    public int setbScore(String bColor, int bWidth) {
        try {
            return dao.getBrickScore(bColor, bWidth);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
}
