package be.howest.groep10.BOL.Game;

import be.howest.groep10.UIL.PnlGameOver;
import be.howest.groep10.BOL.Powers.Power;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Arry on 12/11/2014.
 */
public abstract class GameBoard extends JPanel implements ActionListener, KeyListener{
    private int width = GameResizer.resizeInt(1000), height = GameResizer.resizeInt(720) ;
    private FrmMain frmMain;
    protected final List<Ball> ballList = new ArrayList<Ball>();
    protected List<Paddle> paddleList;
    protected List<Brick> bricks;
    protected List<Power> powers;
    protected Score score;
    protected LevelBuilder levelBuilder;
    protected Difficulty diff;
    protected List<ScoreObserver> scoreBoards;
    private BufferedImage img;

    private int maxLevel;

    protected boolean play = true;
    protected boolean reversedControls = false;
    protected boolean tmpPowerActive = false;
    Timer t = new Timer(5,this);
    java.util.Timer reverseControlsTimer = new java.util.Timer();

    public GameBoard(FrmMain frmMain, Difficulty difficulty)
    {
        super();

        setPreferredSize(new Dimension(width, height));
        setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 5));
        setBackground(Color.LIGHT_GRAY);

        this.frmMain = frmMain;
        scoreBoards = new ArrayList<ScoreObserver>();
        diff = difficulty;
        createComponents();
        startGame();
    }

    public void createComponents() {
        paddleList = new ArrayList<Paddle>();
        score = new Score(diff);
        maxLevel = score.getMaxLevel();
        levelBuilder = new LevelBuilder(this, getScore());
        fillNewBrickList();
        createAdditionalComponents();
        try {
            img = ImageIO.read(new File("images/background.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected abstract void createAdditionalComponents();

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(img, 0, 0, width, height,null);
        drawPause(g);
        for(Brick b : bricks)
        {
            b.draw(g);
        }
        for( Ball b : ballList)
        {
            b.draw(g);
        }
        for(Paddle pdl : paddleList)
        {
            pdl.draw(g);
        }
        drawPowers(g);
    }

    protected abstract void drawPowers(Graphics g);
    protected abstract void drawPause(Graphics g);

    public void actionPerformed(ActionEvent e) {
        ballActions();
        for(Paddle p : paddleList)
        {
            p.move();
        }
        catchPowers();
        notifyObservers();
        repaint();

        checkGameLost();
        checkGameWon();
        checkLevelComplete();
    }
    protected abstract void catchPowers();
    protected abstract void ballActions();
    protected abstract void ballBrickInteraction(Ball b);

    protected void bounceBall(Ball b, ArrayList<Brick> bs)
    {
        if (bs.size() == 1) {
            bounceBall(b, bs.get(0));
        }else if(bs.size() == 2) {
            //here we hit 2 bricks at once
            Brick combinedBrick = bs.get(0).merge(bs.get(1));
            bounceBall(b ,combinedBrick);
        }
    }

    protected void bounceBall(Ball b, Brick br)
    {
        b.checkCollision(br);
    }


    @Override
    public void keyPressed(KeyEvent e) {
        checkArrowKeysPressed(e);
        checkQorDPressed(e);
        if  (e.getKeyCode() == KeyEvent.VK_P)
        {
            if(play)
            {
                play = false;
                stopGame();
                repaint();
            }
        }
        if  (e.getKeyCode() == KeyEvent.VK_SPACE) {
            if(!play) {
                play = true;
                startGame();
                repaint();
            }
        }
    }
    protected abstract void checkArrowKeysPressed(KeyEvent e);
    protected abstract void checkQorDPressed(KeyEvent e);

    public void keyReleased(KeyEvent e) {
        checkArrowKeysReleased(e);
        checkQorDReleased(e);
    }
    protected abstract void checkQorDReleased(KeyEvent e);
    protected abstract void checkArrowKeysReleased(KeyEvent e);

    @Override
    public void keyTyped(KeyEvent e) {

    }


    protected void checkGameWon() {
        if(score.getLevel() > maxLevel)
        {
            stopGame();
            switchToPanel(new PnlGameOver(frmMain, getScore(), diff, false));
        }
    }

    protected void checkGameLost() {
        if(score.getLivesLeft() == 0)
        {
            notifyObservers();
            stopGame();
            switchToPanel(new PnlGameOver(frmMain, getScore(), diff, isMultiplayer()));
        }
    }

    protected void checkLevelComplete() {
        if(bricks.size() <= 0 && score.getLivesLeft() > 0 && score.getLevel() <= maxLevel)
        {
            int finalScore = score.getFinalScore();
            notifyObservers();
            msgbox("Level complete! Score: " + finalScore);
            proceedToNextLevel();
        }
    }

    protected void proceedToNextLevel() {
        score.nextLevel();
        if(score.getLevel() <= 10)
        {
            fillNewBrickList();
            resetBoard();
            notifyObservers();
            startGame();
        }
    }
    protected abstract void fillNewBrickList();

    private void msgbox(String s)
    {
        JOptionPane.showMessageDialog(null, s);
    }

    protected void notifyObservers()
    {
        for (ScoreObserver observer : scoreBoards)
        {
            observer.update();
        }
    }

    public void addObserver(ScoreObserver observer)
    {
        scoreBoards.add(observer);
        notifyObservers();
    }

    public void removeObserver(ScoreObserver observer)
    {
        scoreBoards.remove(observer);
    }


    public void loseLife() {
        score.loseLife();
        resetBoard();
    }

    public void resetBoard() {
        for(Paddle p : paddleList)
        {
            p.reset();
        }
        resetBallProperties();
        for(Ball b : ballList)
        {
            b.reset();
        }
        removePowers();
        score.clearTempTimer();
        tmpPowerActive = false;
        reversedControls = false;
    }

    protected abstract void removePowers();
    protected abstract void resetBallProperties();


    public void stopGame()
    {
        t.stop();
    }

    public void startGame()
    {
        t.start();
    }


    public void reverseControls() {
        if(!tmpPowerActive) {
            tmpPowerActive = true;
            reversedControls = true;
            for(Paddle p : paddleList) {
                p.stopMoving();
            }
            score.startTmpTimer();
            reverseControlsTimer.schedule(
                    new java.util.TimerTask() {
                        @Override
                        public void run() {
                            reversedControls = false;
                            score.clearTempTimer();
                            for(Paddle p : paddleList) {
                                p.stopMoving();
                            }
                            tmpPowerActive = false;
                        }
                    }, 10000);
        }
    }

    protected void switchToPanel(JPanel panel) {
        frmMain.getContentPane().removeAll();
        frmMain.setContentPane(panel);
        frmMain.validate();
        frmMain.repaint();
    }

    protected abstract void loseBall(Ball ball);
    protected abstract boolean isMultiplayer();

    protected boolean ballListContainsBall(Ball b)
    {
        synchronized (ballList) {
            return ballList.contains(b);
        }
    }

    protected void addToBallList(Ball b)
    {
        synchronized (ballList)
        {
            ballList.add(b);
        }
    }

    protected void removeFromBallList(Ball b)
    {
        synchronized (ballList)
        {
            ballList.remove(b);
        }
    }

    public List<Ball> getBallList() {
        return ballList;
    }

    public Score getScore() {
        return score;
    }

    public List<Paddle> getPaddles() {
        return paddleList;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}