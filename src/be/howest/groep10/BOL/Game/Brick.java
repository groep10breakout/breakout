package be.howest.groep10.BOL.Game;

import be.howest.groep10.BOL.Powers.Power;
import be.howest.groep10.BOL.Powers.*;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.util.Random;

/**
 *
 * @author Thomas
 */

public class Brick {
    private int x,y, width;
    private boolean alive;
    private Power power;
    private Color color;
    private int score;
    private static int heigth = GameResizer.resizeInt(35);

    public Brick(int x, int y, int width, Color color, int score)
    {
        this.x = x;
        this.y = y;
        addPower(new NullPower(this.getX(), this.getY()));
        setWidth(width);
        setBgColor(color);
        setScore(score);
        alive = true;
    }

    public void draw(Graphics g)
    {
        g.setColor(getBgColor());
        g.fillRect(x, y, getWidth(), getHeigth());
        g.setColor(Color.BLACK);
        g.drawRect(x, y, getWidth(), getHeigth());
    }

    public void addPower(Power power)
    {
        this.power = power;
    }

    public Brick
    merge(Brick other) {
        return new Brick(this.getX(), this.getY(), this.getWidth() + other.getWidth(), this.getBgColor(), this.getScore() + other.getScore());
    }

    public void destroy()
    {
        alive = false;
    }

    private void setScore(int score)
    {
        this.score = score;
    }

    private void setBgColor(Color color)
    {
        this.color = color;
    }

    private void setWidth(int width) {
        this.width = width;
    }

    /**
     *
     * @return Returns the power attached to the brick
     */
    public Power getPower()
    {
        return power;
    }

    /**
     * @return returns the color of the brick
     */
    private Color getBgColor()
    {
        return color;
    }

    public int getScore()
    {
        return score;
    }

    public int getX()
    {
        return x;
    }

    public int getY()
    {
        return y;
    }

    public int getWidth()
    {
        return width;
    }

    public static int getHeigth() {
        return heigth;
    }

    public Rectangle2D.Double getVirtualBrick()
    {
        return new Rectangle2D.Double(x, y, width , heigth);
    }

}
