package be.howest.groep10.BOL.Game;

import be.howest.groep10.DAL.BreakoutDAO;

/**
 * Created by Arry on 1/12/2014.
 * \m/
 */
public class Difficulty {

    public final static int EASY = 0;
    public final static int MEDIUM = 1;
    public final static  int HARD = 2;
    private int initBallSpeed;
    private int initLives;
    private int currentDifficulty;

    private BreakoutDAO dao;

    public Difficulty(int difficulty) {
        currentDifficulty = difficulty;
        try {
            dao = Launcher.dao;
            setInitLives();
            setInitBallSpeed();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public double getDifficultyMultiplier()
    {
        if(currentDifficulty == Difficulty.EASY)
            return 1;
        else if(currentDifficulty == Difficulty.MEDIUM)
            return 1.5;
        else
            return 2;
    }

    public int getDifficulty()
    {
        return currentDifficulty;
    }

    public int getInitLives()
    {
        return initLives;
    }

    public int getInitBallSpeed() {
        return initBallSpeed;
    }

    private void setInitLives() throws Exception {
        initLives = dao.getInitLives(currentDifficulty + 1);
    }

    private void setInitBallSpeed() throws Exception {
        initBallSpeed = dao.getInitBallSpeed(currentDifficulty + 1);
    }
}
