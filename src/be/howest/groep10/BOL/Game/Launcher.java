package be.howest.groep10.BOL.Game;

import be.howest.groep10.DAL.BreakoutDAO;

/**
 * Created by Arry on 6/11/2014.
 */
public class Launcher  {
    public static BreakoutDAO dao;

    static {
        try {
            dao = new BreakoutDAO();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {
        Launcher main = new Launcher();
        main.Run();
    }

    private void Run()
    {
        FrmMain frmMain = new FrmMain();
    }
}
