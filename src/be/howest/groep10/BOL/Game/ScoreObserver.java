package be.howest.groep10.BOL.Game;

/**
 * Created by Arry on 23/11/2014.
 */
public interface ScoreObserver {
    public void update();
}
