package be.howest.groep10.BOL.Game;

import java.awt.*;

/**
 * Created by Siebe on 17/12/2014.
 */
public class GameResizer {

    private static GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
    private static int width = gd.getDisplayMode().getWidth();

    public static int resizeInt(int i) {
        if(width <= 800)
            i = (i*60)/100;
        else if(width < 1280)
            i = (i*60)/100;
        else if( width < 1920)
            i = (i*80) /100;
        return i;
    }
}
