package be.howest.groep10.BOL.Game;

import be.howest.groep10.BOL.PaddleStatePattern.PaddleSizeMinusOneState;
import be.howest.groep10.BOL.PaddleStatePattern.PaddleSizeNormalState;
import be.howest.groep10.BOL.PaddleStatePattern.PaddleSizeState;
import be.howest.groep10.BOL.Powers.Power;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by Arry on 6/11/2014.
 */
public class Paddle {

    private int width = 170, height = 10, x = 0, y=0;
    public boolean goingLeft=false, goingRight=false;
    private BufferedImage imgPlayer1;
    private BufferedImage imgPlayer2;
    private GameBoard board;
    private PaddleSizeState state;
    private int difficulty;
    private boolean player2 = false;

    public Paddle(GameBoard panel, Difficulty difficulty, boolean player2)
    {
        this.board = panel;
        this.difficulty = difficulty.getDifficulty();
        this.player2 = player2;
        reset();
    }

    public void move()
    {
        if(goingLeft)
            x -= 4;
        if(goingRight)
            x += 4;
        if(x < 0)
            x = 0;
        if(x+width > board.getWidth())
            x = board.getWidth()-width;
    }

    public void draw(Graphics g)
    {
        if (player2) {
            g.drawImage(imgPlayer2, x, y, width, height, null);
        } else {
            g.drawImage(imgPlayer1, x, y, width, height,  null);
        }
    }

    public void reset()
    {
        if(difficulty == Difficulty.EASY) {
            setState(new PaddleSizeNormalState(this));
        }
        if(difficulty == Difficulty.MEDIUM || difficulty == Difficulty.HARD) {
            setState(new PaddleSizeMinusOneState(this));
        }
        stopMoving();
        x = (board.getWidth()/2) - (width/2);
        if (player2) {
            y = 10;
        } else {
            y = board.getHeight() - 20;
        }
    }

    public void PaddleSizeDecrease() {
        state.decreaseSize();
    }

    public void PaddleSizeIncrease() {
        state.increaseSize();
    }

    public void setState(PaddleSizeState state)
    {
        this.state = state;
    }

    public void setImagePlayer1(String path)
    {
        try
        {
            imgPlayer1 = ImageIO.read(new File(path));
        }
        catch (IOException ie)
        {
            System.out.println("Paddle image not loaded correctly");
        }
    }

    public void setImagePlayer2(String path)
    {
        try
        {
            imgPlayer2 = ImageIO.read(new File(path));
        }
        catch (IOException ie)
        {
            System.out.println("Paddle image not loaded correctly");
        }
    }

    public boolean isPlayer2()
    {
        return player2;
    }

    public int getWidth()
    {
        return width;
    }

    public void setWidth(int width)
    {
        this.width = GameResizer.resizeInt(width);
    }

    public int getX()
    {
        return x;
    }

    public int getY()
    {
        return y;
    }

    public int getHeight() {
        return height;
    }

    public void catchPower(Power power)
    {
        Rectangle2D.Double powerRect = power.getVirtualPower();
        Rectangle2D.Double self = new Rectangle2D.Double(getX(), getY(), getWidth(), getHeight());
        if(self.intersects(powerRect))
        {
            if (isPlayer2()) {
                power.activatePowerForPlayer2();
            } else {
                power.activatePowerForPlayer1();
            }
            power.remove();
        }
    }

    public void stopMoving() {
        goingRight = false;
        goingLeft = false;
    }

    public Rectangle2D.Double getVirtualPaddle()
    {
        return new Rectangle2D.Double(getX(), getY(), getWidth(), getHeight());
    }
}