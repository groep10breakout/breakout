package be.howest.groep10.BOL.Game;

import be.howest.groep10.UIL.PnlMenu;

import javax.swing.*;

/**
 * Created by Siebe on 6/11/2014.
 */
public class FrmMain extends JFrame {


    public FrmMain() {
        super();

        setTitle("Breakout");
        setContentPane(new PnlMenu(this));
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setVisible(true);
        setLocationRelativeTo(null);
    }
}
