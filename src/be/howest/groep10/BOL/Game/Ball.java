package be.howest.groep10.BOL.Game;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;


/**
 * Created by Arry on 6/11/2014.
 */
public class Ball {

    private int x, y, radius = 15, xspeed, yspeed, diameter = radius*2;
    private GameBoard board;
    private BufferedImage img;
    private Difficulty difficulty;
    private Paddle owner;

    public Ball(GameBoard panel, Difficulty difficulty, Paddle owner)
    {
        board = panel;
        this.difficulty = difficulty;
        setOwner(owner);
        reset();
    }

    public void draw(Graphics g)
    {
        g.drawImage(img, x-radius, y-radius, diameter, diameter, null);
        //x,y coordinaat is midden van de bal
    }

    public void setOwner(Paddle paddle)
    {
        owner = paddle;
    }

    public Paddle getOwner()
    {
        return owner;
    }

    public void setNormalBall()
    {
        try
        {
            img = ImageIO.read(new File("images/normalBall.png"));
            radius = GameResizer.resizeInt(15);
            diameter = radius*2;
        }
        catch (IOException ie)
        {
            System.out.println("normalBall image not loaded correctly");
        }
    }

    public void setMegaBall()
    {
        try
        {
            img = ImageIO.read(new File("images/megaBall.png"));
            radius = GameResizer.resizeInt(20);
            diameter = radius*2;
        }
        catch (IOException ie)
        {
            System.out.println("megaBall image not loaded correctly");
        }
    }

    public void setMiniBall()
    {
        try
        {
            img = ImageIO.read(new File("images/miniBall.png"));
            radius = GameResizer.resizeInt(10);
            diameter = radius*2;
        }
        catch (IOException ie)
        {
            System.out.println("miniBall image not loaded correctly");
        }
    }

    public void move()
    {

        x += xspeed;
        y += yspeed;

        checkLeftAndRight();
        checkTop();
        checkBottom();
    }

    private void checkTop()
    {
        if(y-radius < 0) {
            if(!board.isMultiplayer()) {
                y = radius;
                yspeed = Math.abs(yspeed);
            }
            else
            {
                board.loseBall(this);
            }
        }
    }

    private void checkBottom()
    {
        if(y >= board.getHeight())
        {
            board.loseBall(this);
        }
    }

    private void checkLeftAndRight()
    {
        if(x-radius < 0)
        {
            x = x + radius;
            xspeed = Math.abs(xspeed);
        }
        if(x+radius > board.getWidth()) {
            x = board.getWidth() - radius;
            xspeed = -Math.abs(xspeed);
        }
    }

    public void reset()
    {
        x = board.getWidth()/2;
        if (owner.isPlayer2()) {
            y = GameResizer.resizeInt(70);
        } else {
            y = GameResizer.resizeInt(650);
        }

        setNormalBall();
        if (owner.isPlayer2()) {
            setSpeeds(1, difficulty.getInitBallSpeed());
        } else {
            setSpeeds(1, -difficulty.getInitBallSpeed());
        }
    }

    public void checkCollision(Brick b)
    {
        if(y > b.getY() && y < b.getY() + b.getHeigth())
            xspeed = -xspeed;
        else if(x+radius > b.getX() && x-radius < b.getX() + b.getWidth()) {
            yspeed = -yspeed;
        }
    }

    public void checkPaddleCollision(Paddle paddle) {
        Rectangle2D.Double self = getVirtualBall();
        Rectangle2D.Double p = paddle.getVirtualPaddle();
        if (self.intersects(p)) {
            if (x + radius > paddle.getX() && x - radius < paddle.getX() + paddle.getWidth()) {
                if (paddle.isPlayer2()) {
                    if (y - radius <= paddle.getY()) {
                        y = paddle.getY() + radius;
                        int newYSpeed = yspeed *= -1;
                        int newXspeed = calculateNewXSpeed(paddle);
                        setSpeeds(newXspeed, newYSpeed);
                    }
                } else {
                    if (y + radius >= paddle.getY()) {
                        y = paddle.getY() - radius;
                        int newYSpeed = yspeed *= -1;
                        int newXspeed = calculateNewXSpeed(paddle);
                        setSpeeds(newXspeed, newYSpeed);
                    }
                }
                setOwner(paddle);
            }
        }
    }

    private int calculateNewXSpeed(Paddle paddle)
    {
        int halfPaddleWidth = paddle.getWidth() / 2;
        int divider = halfPaddleWidth / 3;
        int dist = x - (paddle.getX() + halfPaddleWidth);
        int newXspeed = dist / divider;
        return newXspeed;
    }

    private void setSpeeds(int xspeed, int yspeed)
    {
        this.xspeed = xspeed;
        this.yspeed = yspeed;
    }

    public void setSlowSpeed()
    {
        int newYspeed = 2;

        if(yspeed <0)
            newYspeed *= -1;

        setSpeeds(xspeed, newYspeed);
    }

    public void setFastSpeed()
    {
        int newYspeed = 4;

        if(yspeed < 0)
            newYspeed *= -1;

        setSpeeds(xspeed, newYspeed);
    }

    public Rectangle2D.Double getVirtualBall()
    {
        return new Rectangle2D.Double(x - radius, y-radius, diameter, diameter);
    }
}