package be.howest.groep10.BOL.Game;


import be.howest.groep10.BOL.Powers.*;


import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Rectangle2D;
import java.util.*;

/**
 * Created by Arry on 16/11/2014.
 */
public class SPGameBoard extends GameBoard {

    private Paddle paddle;
    private Ball ball;

    public SPGameBoard(FrmMain frmMain, Difficulty difficulty)
    {
        super(frmMain, difficulty);
    }

    @Override
    protected void createAdditionalComponents() {
        paddle = new Paddle(this, diff, false);
        paddleList.add(paddle);
        ball = new Ball(this, diff, paddle);
        addToBallList(ball);
        powers = new ArrayList<Power>();

    }

    @Override
    protected void drawPowers(Graphics g) {
        for(Power p : powers)
        {
            p.fallDown(g);
        }
    }

    @Override
    protected void drawPause(Graphics g) {
        if(!play)
        {
            g.setFont(new Font("Impact", Font.PLAIN, GameResizer.resizeInt(35)));
            g.setColor(Color.WHITE);
            g.drawString("Game Paused: hit space to continue",GameResizer.resizeInt(250),GameResizer.resizeInt(400));
        }
    }

    @Override
    protected void catchPowers() {
        for(Power p : powers)
        {
            paddle.catchPower(p);
        }
    }

    @Override
    protected void ballActions() {
        for(Ball b : ballList)
        {
            b.move();
            ballBrickInteraction(b);
            b.checkPaddleCollision(paddle);
        }
    }

    @Override
    protected void ballBrickInteraction(Ball b)
    {
        Rectangle2D.Double bal = b.getVirtualBall();
        ArrayList<Brick> toBeRemoved = new ArrayList<Brick>();
        for(Brick brick : bricks)
        {
            Rectangle2D.Double br = brick.getVirtualBrick();
            if(br.intersects(bal))
            {
                toBeRemoved.add(brick);
                powers.add(brick.getPower());
            }
        }
        bounceBall(b, toBeRemoved);
        for(Brick br : toBeRemoved)
        {
            if (b.getOwner().isPlayer2()) {
                score.addScore2(br.getScore());
            } else {
                score.addScore1(br.getScore());
            }
            br.destroy();
            bricks.remove(br);
        }
    }

    @Override
    protected void checkArrowKeysPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            if (reversedControls) {
                paddle.goingRight = true;
            } else {
                paddle.goingLeft = true;
            }
        }
        if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            if (reversedControls) {
                paddle.goingLeft = true;
            } else {
                paddle.goingRight = true;
            }
        }
    }

    @Override
    protected void checkArrowKeysReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            if (reversedControls) {
                paddle.goingRight = false;
            } else {
                paddle.goingLeft = false;
            }
        }
        if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            if (reversedControls) {
                paddle.goingLeft = false;
            } else {
                paddle.goingRight = false;
            }
        }
    }

    @Override
    protected void checkQorDPressed(KeyEvent e) {

    }

    @Override
    protected void checkQorDReleased(KeyEvent e) {

    }

    @Override
    protected void fillNewBrickList() {
        bricks = levelBuilder.fillBrickList();
    }

    @Override
    protected void removePowers() {
        for(Power p : powers) {
            p.remove();
        }
    }

    @Override
    protected void resetBallProperties() {
        ball.setOwner(paddle);
        if(!ballListContainsBall(ball))
           addToBallList(ball);
    }

    @Override
    protected boolean isMultiplayer() {
        return false;
    }

    @Override
    public void loseBall(Ball ball) {
        stopGame();
        removeFromBallList(ball);
        if(ballList.size() <= 0)
            loseLife();
        startGame();
    }
}