package be.howest.groep10.BOL.Game;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Arry on 23/11/2014.
 */
public class Score {

    private int nrOfLives = 0;
    private int score1 = 0;
    private int score2 = 0;
    private int totalScore = 0;
    private int level = 1;
    private Difficulty difficulty;
    private int tmpTimerSeconds;
    private Timer t = new Timer();
    private int maxLevel;

    public Score(Difficulty difficulty)
    {
        this.difficulty = difficulty;
        setStartingLives();
        score1 = 0;
        score2 = 0;
        totalScore = 0;
    }

    private void setStartingLives()
    {
        nrOfLives = difficulty.getInitLives();
    }

    public int getLivesLeft()
    {
        return nrOfLives;
    }

    public void loseLife()
    {
        nrOfLives--;
    }

    public void addScore1(int amount)
    {
        if(score1 + amount < 0)
            score1 = 0;
        else
            score1 += amount;
        addTotalScore(amount);
    }

    public void addScore2(int amount)
    {
        if(score2 + amount < 0)
            score2 = 0;
        else
            score2 += amount;
        addTotalScore(amount);
    }

    private void addTotalScore(int amount)
    {
        if(totalScore + amount < 0)
            totalScore =0;
        else
            totalScore += amount;
    }

    public int getTotalScore()
    {
        return totalScore;
    }

    public int getScore1()
    {
        return score1;
    }

    public int getScore2() { return score2; }

    public void extraLife()
    {
        if(nrOfLives + 1 <= 5)
            nrOfLives++;
    }

    /**
     *
     * @return Returns the totalScore, with 50 points extra per life
     */
    public int getFinalScore()
    {
        totalScore += nrOfLives * 50;
        return totalScore;
    }

    public int getEndScore1()
    {
        return (int)Math.round(getScore1() * difficulty.getDifficultyMultiplier());
    }

    public int getEndScore2()
    {
        return (int)Math.round(getScore2() * difficulty.getDifficultyMultiplier());
    }

    public int getEndFinalScore()
    {
        return (int)Math.round(totalScore * difficulty.getDifficultyMultiplier());
    }

    public int getLevel()
    {
        return  level;
    }

    public void nextLevel()
    {
        level ++;
    }

    /**
     * Starts a timer that decreases with 1 every second for 10 seconds (10, 9, ... 1)
     */
    public void startTmpTimer() {
        t = new Timer();
        tmpTimerSeconds =10;
            t.schedule(new TimerTask() {
                @Override
                public void run() {
                    if (tmpTimerSeconds - 1 >= 0)
                        tmpTimerSeconds--;
                }
            }, 1000, 1000);
    }

    public int getTmpTimerSeconds()
    {
        return tmpTimerSeconds;
    }

    /**
     * Cancels the tmpTimer and puts its seconds back to 0
     */
    public void clearTempTimer() {
        t.cancel();
        tmpTimerSeconds = 0;
    }

    public int getMaxLevel() {
        try {
            maxLevel =  Launcher.dao.getMaxLevel();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return maxLevel;
    }
}
