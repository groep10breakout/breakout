package be.howest.groep10.UIL;

import be.howest.groep10.BOL.Game.FrmMain;
import be.howest.groep10.BOL.Game.GameResizer;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by Siebe on 6/11/2014.
 */
public class PnlMenu extends JPanel {
    private JButton btnPlaySP, btnPlayMP, btnInstructions, btnHSSP, btnHSMP;
    private FrmMain frmMain;
    private BufferedImage bgImg;
    public PnlMenu(FrmMain frmMain) {
        super();

        setLayout(new GridLayout(5, 1, 0, GameResizer.resizeInt(25)));
        setPreferredSize(new Dimension(GameResizer.resizeInt(1280), GameResizer.resizeInt(720)));
        setBorder(BorderFactory.createEmptyBorder(GameResizer.resizeInt(200), GameResizer.resizeInt(400), GameResizer.resizeInt(100), GameResizer.resizeInt(400)));

        this.frmMain = frmMain;
        createComponents();
        addComponents();
        addActionListeners();
        repaint();
    }

    private void createComponents() {
        btnPlaySP = new JButton("SINGLE PLAYER");
        btnPlayMP = new JButton("MULTI PLAYER");
        btnInstructions = new JButton("INSTRUCTIONS");
        btnHSSP = new JButton("HIGH SCORES (SP)");
        btnHSMP = new JButton("HIGH SCORES (MP)");
        setComponentLayout(btnPlaySP);
        setComponentLayout(btnPlayMP);
        setComponentLayout(btnInstructions);
        setComponentLayout(btnHSSP);
        setComponentLayout(btnHSMP);
        try {
            bgImg = ImageIO.read(new File("images/menuBG.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void addComponents() {
        add(btnPlaySP);
        add(btnPlayMP);
        add(btnInstructions);
        add(btnHSSP);
        add(btnHSMP);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(bgImg,0,0,frmMain.getWidth(),frmMain.getHeight(),null);
    }

    private void addActionListeners() {
        btnPlaySP.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                switchToPanel(new PnlDifficulty(frmMain, false));
            }
        });
        btnPlayMP.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                switchToPanel(new PnlDifficulty(frmMain, true));
            }
        });
        btnInstructions.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                switchToPanel(new PnlInstructions(frmMain));
            }
        });
        btnHSSP.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                switchToPanel(new PnlHSSP(frmMain));
            }
        });
        btnHSMP.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                switchToPanel(new PnlHSMP(frmMain));
            }
        });
    }

    private void setComponentLayout(JComponent cmp) {
        cmp.setFont(new Font("Impact", Font.PLAIN, 30));
        cmp.setBackground(Color.BLACK);
        cmp.setForeground(Color.YELLOW);
        cmp.setFocusable(false);
    }

    private void switchToPanel(JPanel panel) {
        frmMain.getContentPane().removeAll();
        frmMain.setContentPane(panel);
        frmMain.validate();
        frmMain.repaint();
    }
}
