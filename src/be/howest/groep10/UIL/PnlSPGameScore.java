package be.howest.groep10.UIL;

import be.howest.groep10.BOL.Game.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by Siebe on 6/11/2014.
 */
public class PnlSPGameScore extends JPanel implements ScoreObserver {
    private SPGameBoard gameBoard;
    private JButton btnMenu;
    private JLabel lblLives, lblScore, lblTmpPowers, lblLevel, lblEmptySpace;
    private FrmMain frmMain;
    private int score =0, nrOfLives, level =1, tempPower = 10;
    private Score model;
    BufferedImage imgLives;
    BufferedImage imgReverseControls;

    private int width = GameResizer.resizeInt(280), heigth = GameResizer.resizeInt(720);

    public PnlSPGameScore(FrmMain frmMain, SPGameBoard gameBoard) {
        super();

        this.frmMain = frmMain;
        ((FlowLayout)this.getLayout()).setVgap(0);
        setBackground(Color.GRAY);
        setPreferredSize(new Dimension(width, heigth));

        this.model = gameBoard.getScore();
        this.gameBoard = gameBoard;
        createComponents();
        addComponents();
        addActionListeners();
        this.gameBoard.addObserver(this);

        try
        {
            imgLives = ImageIO.read(new File("images/life.png"));
            imgReverseControls = ImageIO.read(new File("images/reverseControls.png"));
        }
        catch (IOException ie)
        {
            System.out.println("Some image in the SPGameScore class did not load correctly");
        }
    }

    private void createComponents() {
        lblLives = new JLabel(" Lives:", SwingConstants.LEFT);
        lblScore = new JLabel("", SwingConstants.RIGHT);
        lblScore.setOpaque(true);
        lblLevel = new JLabel(" Level: " + level);
        lblTmpPowers = new JLabel();
        lblEmptySpace = new JLabel();
        btnMenu = new JButton("Back to menu");
        addComponentLayout(lblScore, getWidth(), GameResizer.resizeInt(50), 25, Color.YELLOW, Color.DARK_GRAY);
        addComponentLayout(lblLives, getWidth(),GameResizer.resizeInt(50),30,Color.DARK_GRAY, Color.GRAY);
        addComponentLayout(lblTmpPowers, getWidth(), GameResizer.resizeInt(50), 30, Color.DARK_GRAY, Color.GRAY);
        addComponentLayout(lblLevel, getWidth(), GameResizer.resizeInt(50), 30, Color.DARK_GRAY, Color.GRAY);
        addComponentLayout(lblEmptySpace, getWidth(), GameResizer.resizeInt(470), 15, Color.DARK_GRAY, Color.GRAY);
        addComponentLayout(btnMenu, getWidth(), GameResizer.resizeInt(50), 20, Color.YELLOW, Color.BLACK);
    }

    private void addComponents() {
        add(lblScore);
        add(lblLives);
        add(lblTmpPowers);
        add(lblLevel);
        add(lblEmptySpace);
        add(btnMenu);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        int xPos = GameResizer.resizeInt(245);
        for(int i = 0; i < nrOfLives; i++)
        {
            g.drawImage(imgLives,xPos,55,30, 30, null);
            xPos -= GameResizer.resizeInt(35);
        }
        if(tempPower > 0)
            g.drawImage(imgReverseControls, GameResizer.resizeInt(30), GameResizer.resizeInt(110), 30,30, null);
    }

    private void addActionListeners() {
        btnMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                gameBoard.stopGame();
                switchToPanel(new PnlMenu(new FrmMain()));
                frmMain.dispose();
            }
        });
    }

    private void addComponentLayout(JComponent cmp, int width, int height, int fontSize, Color fgColor, Color bgColor) {
        cmp.setPreferredSize(new Dimension(width, height));
        cmp.setFont(new Font("Impact", Font.PLAIN, fontSize));
        cmp.setForeground(fgColor);
        cmp.setBackground(bgColor);
        cmp.setFocusable(false);
    }

    public void switchToPanel(JPanel panel) {
        frmMain.getContentPane().removeAll();
        frmMain.setContentPane(panel);
        frmMain.validate();
        frmMain.repaint();
    }

    @Override
    public void update() {
        score = model.getTotalScore();
        nrOfLives = model.getLivesLeft();
        lblLevel.setText(" Level: " + model.getLevel());
        lblScore.setText(Integer.toString(score));
        tempPower = model.getTmpTimerSeconds();
        if(tempPower <= 0)
            lblTmpPowers.setText("");
        else
            lblTmpPowers.setText(Integer.toString(tempPower));

        repaint();
    }

    public int getWidth()
    {
        return width;
    }

    public int getHeigth()
    {
        return heigth;
    }
}
