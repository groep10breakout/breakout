package be.howest.groep10.UIL;

import be.howest.groep10.BOL.Game.FrmMain;
import be.howest.groep10.BOL.Game.GameResizer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Siebe on 6/11/2014.
 */
public class PnlInstructions extends JPanel {
    private JLabel lblTitle;
    private JPanel pnlCenter;
    private JButton btnMenu;
    private FrmMain frmMain;

    public PnlInstructions(FrmMain frmMain) {
        super();

        setLayout(new BorderLayout());
        setBackground(Color.GRAY);

        this.frmMain = frmMain;

        createComponents();
        addComponents();
        addActionListeners();
    }

    private void createComponents() {
        lblTitle = new JLabel("INSTRUCTIONS", SwingConstants.CENTER);
        lblTitle.setOpaque(true);

        pnlCenter = new PnlInstructionsCenter();

        btnMenu = new JButton("Back to menu");

        addComponentLayout(lblTitle, 100);
        addComponentLayout(btnMenu, 75);
    }

    private void addComponents() {
        add(lblTitle, BorderLayout.NORTH);
        add(pnlCenter, BorderLayout.CENTER);
        add(btnMenu, BorderLayout.SOUTH);
    }

    private void addActionListeners() {
        btnMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                switchToPanel(new PnlMenu(new FrmMain()));
                frmMain.dispose();
            }
        });
    }

    private void addComponentLayout(JComponent cmp, int height) {
        cmp.setPreferredSize(new Dimension(GameResizer.resizeInt(200), GameResizer.resizeInt(height)));
        cmp.setFont(new Font("Impact", Font.PLAIN, 30));
        cmp.setBackground(Color.BLACK);
        cmp.setForeground(Color.YELLOW);
    }

    public void switchToPanel(JPanel panel) {
        frmMain.getContentPane().removeAll();
        frmMain.setContentPane(panel);
        frmMain.validate();
        frmMain.repaint();
    }
}
