package be.howest.groep10.UIL;

import be.howest.groep10.BOL.Game.GameResizer;
import be.howest.groep10.DAL.BreakoutDAO;
import be.howest.groep10.BOL.Game.Launcher;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by Siebe on 6/11/2014.
 */
public class PnlInstructionsCenter extends JPanel {
    private JLabel lblControlsTitle, lblControlsSP, lblControlsMP, lblPause, lblPowerUpsTitle, lblPowerDownsTitle;
    private String[] columnTitles = {"icon", "description"};
    protected BreakoutDAO breakoutDAO;
    private Object[][] powerUps;
    private Object[][] powerDowns;
    private JTable tblPowerUps, tblPowerDowns;
    private BufferedImage img;
    private List<String> powerUpImageList;
    private List<String> powerDownImageList;
    private List<String> powerUpDescriptionList;
    private List<String> powerDownDescriptionList;

    private BufferedImage bgImg;

    public PnlInstructionsCenter() {
        super();

        createComponents();
        addComponents();
    }

    @Override
    protected void paintComponent(Graphics g) {
        g.drawImage(bgImg, 0,0, GameResizer.resizeInt(1280), GameResizer.resizeInt(720), null);
        drawImages(powerUpImageList, 3, 215, g);
        drawImages(powerDownImageList, 643, 215, g);
    }

    private void drawImages(List<String> powerImageList, int x, int y, Graphics g) {
        for (String power : powerImageList) {
            try {
                img = ImageIO.read(new File((power) + ".png"));
            } catch (IOException ie) {
                System.out.println("Image not loaded correctly");
            } catch (Exception e) {
                e.printStackTrace();
            }
            g.drawImage(img, GameResizer.resizeInt(x), GameResizer.resizeInt(y), GameResizer.resizeInt(30), GameResizer.resizeInt(30), null);
            y += 40;
        }
    }

    private void fillDescription (Object[][] descriptionList, List<String> descriptions)
    {
        for (int i = 0; i < descriptions.size(); i++) {
            descriptionList[i][0] = "";
            descriptionList[i][1] = descriptions.get(i);
        }
    }

    private void createComponents() {
        lblControlsTitle = new JLabel("Controls", SwingConstants.CENTER);
        lblControlsSP = new JLabel("SINGLE PLAYER:  Use arrow keys LEFT and RIGHT to move your paddle.");
        lblControlsMP = new JLabel("MULTI PLAYER:   Player1 uses arrow keys LEFT and RIGHT. Player2 uses 'Q' and 'D'.");
        lblPause = new JLabel("Press 'P' to pause, then press SPACE to unpause.");
        lblPowerUpsTitle = new JLabel("Power-ups", SwingConstants.CENTER);
        lblPowerDownsTitle = new JLabel("Power-downs", SwingConstants.CENTER);
        breakoutDAO = Launcher.dao;

        addComponentLayout(lblControlsTitle, 1280, 50, 25, Color.YELLOW, Color.DARK_GRAY, true);
        addComponentLayout(lblControlsSP, 1280, 20, 16, Color.DARK_GRAY, Color.GRAY, false);
        addComponentLayout(lblControlsMP, 1280, 20, 16, Color.DARK_GRAY, Color.GRAY, false);
        addComponentLayout(lblPause, 1280, 40, 16, Color.DARK_GRAY, Color.GRAY, false);
        addComponentLayout(lblPowerUpsTitle, 630, 50, 25, Color.YELLOW, Color.DARK_GRAY, true);
        addComponentLayout(lblPowerDownsTitle, 630, 50, 25, Color.YELLOW, Color.DARK_GRAY, true);

        fillLists();
        tblPowerUps = new JTable(powerUps, columnTitles);
        tblPowerDowns = new JTable(powerDowns, columnTitles);
        addTableLayout(tblPowerUps, 30, 600, 40, 20);
        addTableLayout(tblPowerDowns, 30, 600, 40, 20);

        try {
            bgImg = ImageIO.read(new File("images/background.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void fillLists()
    {
        try {
            powerUpImageList = breakoutDAO.getPowerList("up");
            powerDownImageList = breakoutDAO.getPowerList("down");
            powerUpDescriptionList = breakoutDAO.getPowerDescriptionList("up");
            powerDownDescriptionList = breakoutDAO.getPowerDescriptionList("down");
        } catch (Exception e) {
            e.printStackTrace();
        }
        powerUps = new Object[powerUpImageList.size()][2];
        powerDowns = new Object[powerDownImageList.size()][2];
        fillDescription(powerUps, powerUpDescriptionList);
        fillDescription(powerDowns, powerDownDescriptionList);
    }

    private void addComponents() {
        add(lblControlsTitle);
        add(lblControlsSP);
        add(lblControlsMP);
        add(lblPause);
        add(lblPowerUpsTitle);
        add(lblPowerDownsTitle);
        add(tblPowerUps);
        add(tblPowerDowns);
    }

    private void addComponentLayout(JComponent cmp, int width, int height, int fontSize, Color fgColor, Color bgColor, boolean isOpaque) {
        cmp.setOpaque(isOpaque);
        cmp.setFont(new Font("Impact", Font.PLAIN, fontSize));
        cmp.setForeground(fgColor);
        cmp.setBackground(bgColor);
        cmp.setPreferredSize(new Dimension(GameResizer.resizeInt(width), GameResizer.resizeInt(height)));
    }

    private void addTableLayout(JTable tbl, int columnWidth1, int columnWidth2, int rowHeight, int fontSize) {
        TableColumn column = null;
        for (int i = 0; i < 2; i++) {
            column = tbl.getColumnModel().getColumn(i);
            if (i == 0) {
                column.setPreferredWidth(GameResizer.resizeInt(columnWidth1));
            } else {
                column.setPreferredWidth(GameResizer.resizeInt(columnWidth2));
            }
        }
        tbl.setRowHeight(GameResizer.resizeInt(rowHeight));
        tbl.setShowGrid(false);
        tbl.setFont(new Font("Impact", Font.PLAIN, fontSize));
        tbl.setForeground(Color.DARK_GRAY);
        tbl.setBackground(new Color(255,255,255,0));
        tbl.setEnabled(false);
    }
}
