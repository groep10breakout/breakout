package be.howest.groep10.UIL;

import be.howest.groep10.BOL.Game.Difficulty;
import be.howest.groep10.BOL.Game.FrmMain;
import be.howest.groep10.BOL.Game.GameResizer;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by Thomas on 1/12/2014.
 */
public class PnlDifficulty extends JPanel {

    private JButton btnEasy, btnMedium, btnHard, btnBack;
    private FrmMain frmMain;
    private boolean multiplayer;
    private BufferedImage bgImg;
    
    public PnlDifficulty(FrmMain frmMain, boolean multiplayer)
    {
        super();

        setLayout(new GridLayout(4,1,0,15));
        setPreferredSize(new Dimension(frmMain.getWidth(), frmMain.getHeight()));
        setBorder(BorderFactory.createEmptyBorder(GameResizer.resizeInt(200), GameResizer.resizeInt(400), GameResizer.resizeInt(200), GameResizer.resizeInt(400)));
        setBackground(Color.GRAY);

        this.frmMain = frmMain;
        this.multiplayer = multiplayer;

        createComponents();
        addComponents();
        addActionListeners();
    }

    private void createComponents() {
        btnEasy = new JButton("Easy");
        btnMedium = new JButton("Medium");
        btnHard = new JButton("Hard");
        btnBack = new JButton("Back to menu");
        setComponentLayout(btnEasy);
        setComponentLayout(btnMedium);
        setComponentLayout(btnHard);
        setComponentLayout(btnBack);
        try {
            bgImg = ImageIO.read(new File("images/menuBG.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void addComponents() {
        add(btnEasy);
        add(btnMedium);
        add(btnHard);
        add(btnBack);
    }

    private void addActionListeners() {

        btnEasy.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (multiplayer) {
                    switchToPanel(new PnlMPGame(frmMain, new Difficulty(Difficulty.EASY)));
                } else {
                    switchToPanel(new PnlSPGame(frmMain, new Difficulty(Difficulty.EASY)));
                }
            }
        });
        btnMedium.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (multiplayer) {
                    switchToPanel(new PnlMPGame(frmMain, new Difficulty(Difficulty.MEDIUM)));
                } else {
                    switchToPanel(new PnlSPGame(frmMain, new Difficulty(Difficulty.MEDIUM)));
                }
            }
        });
        btnHard.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (multiplayer) {
                    switchToPanel(new PnlMPGame(frmMain, new Difficulty(Difficulty.HARD)));
                } else {
                    switchToPanel(new PnlSPGame(frmMain, new Difficulty(Difficulty.HARD)));
                }
            }
        });
        btnBack.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                    switchToPanel(new PnlMenu(frmMain));
            }
        });
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(bgImg, 0,0, frmMain.getWidth(), frmMain.getHeight(), null);
    }

    private void setComponentLayout(JComponent cmp) {
        cmp.setFont(new Font("Impact", Font.PLAIN, 30));
        cmp.setBackground(Color.BLACK);
        cmp.setForeground(Color.YELLOW);
        cmp.setFocusable(false);
    }

    private void switchToPanel(JPanel panel) {
        frmMain.getContentPane().removeAll();
        frmMain.setContentPane(panel);
        frmMain.validate();
        frmMain.repaint();
    }
}
