package be.howest.groep10.UIL;

import be.howest.groep10.BOL.Game.GameResizer;
import be.howest.groep10.DAL.BreakoutDAO;
import be.howest.groep10.DAL.PlayersMP;
import be.howest.groep10.BOL.Game.Launcher;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by Siebe on 8/11/2014.
 */
public class PnlHSMPCenter extends JPanel {
    private String[] columnTitles = {"Rank", "Name", "Score", "Total Score"};
    private List<PlayersMP> playersList;
    private String[][] scoreList;
    private JTable scoreTable;
    private BreakoutDAO dao;

    private BufferedImage bgImg;

    public PnlHSMPCenter() {
        super();

        createComponents();
        addComponents();
    }

    private void createComponents() {
        try {
            dao = Launcher.dao;
            playersList = dao.getMPHighscoreList();
        } catch (Exception e) {
            e.printStackTrace();
        }

        scoreList = new String[playersList.size() * 2][4];

        for (int i = 0; i < playersList.size() * 2; i += 2) {
            scoreList[i][0] = i / 2 + 1 + ".";
            scoreList[i][1] = playersList.get(i/2).getName1();
            scoreList[i][2] = Integer.toString(playersList.get(i/2).getScore1());
            scoreList[i][3] = Integer.toString(playersList.get(i/2).getTotalScore());
            scoreList[i + 1][0] = "";
            scoreList[i + 1][1] = playersList.get(i/2).getName2();
            scoreList[i + 1][2] = Integer.toString(playersList.get(i/2).getScore2());
            scoreList[i + 1][3] = "";
        }
        scoreTable = new JTable(scoreList, columnTitles);

        addComponentLayout(scoreTable.getTableHeader(), 1180, 45, 25, Color.YELLOW, Color.DARK_GRAY, true);
        addComponentLayout(scoreTable,1180,500,18,Color.DARK_GRAY,Color.GRAY, false);
        addTableLayout(scoreTable, 80, 365);

        try {
            bgImg = ImageIO.read(new File("images/background.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void addComponents() {
        add(scoreTable.getTableHeader());
        add(scoreTable);
    }

    private void addComponentLayout(JComponent cmp, int width, int height, int fontSize, Color fgColor, Color bgColor, boolean isOpaque) {
        cmp.setOpaque(isOpaque);
        cmp.setFont(new Font("Impact", Font.PLAIN, GameResizer.resizeInt(fontSize)));
        cmp.setPreferredSize(new Dimension(GameResizer.resizeInt(width), GameResizer.resizeInt(height)));
        cmp.setBackground(bgColor);
        cmp.setForeground(fgColor);
    }

    private void addTableLayout(JTable tbl, int columnWidth1, int columnWidth2) {
        TableColumn column = null;
        for (int i = 0; i < 2; i++) {
            column = tbl.getColumnModel().getColumn(i);
            if (i == 0) {
                column.setPreferredWidth(GameResizer.resizeInt(columnWidth1));
            } else {
                column.setPreferredWidth(GameResizer.resizeInt(columnWidth2));
            }
        }
        tbl.setRowHeight(GameResizer.resizeInt(25));
        tbl.setShowGrid(false);
        tbl.setEnabled(false);
        tbl.setBackground(new Color(255, 255, 255, 0));
        tbl.getTableHeader().setEnabled(false);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(bgImg, 0,0, GameResizer.resizeInt(1280), GameResizer.resizeInt(720), null);
    }
}
