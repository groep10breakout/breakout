package be.howest.groep10.UIL;

import be.howest.groep10.BOL.Game.*;
import be.howest.groep10.DAL.BreakoutDAO;
import be.howest.groep10.DAL.PlayerSP;
import be.howest.groep10.DAL.PlayersMP;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by Thomas on 11/12/2014.
 */
public class PnlGameOver extends JPanel {
    private boolean multiplayer;
    private Score score;
    private Difficulty difficulty;
    private FrmMain frmMain;
    private JButton btnOk;
    private JTextField txtName1, txtName2;
    private JLabel lblTitle, lblPlayer1, lblPlayer2, lblScore1, lblScore2, lblName1, lblName2,
            lblEndscore1, lblEndscore2, lblFinalscore, lblMultiplier1, lblMultiplier2, lblEmptySpace;

    private BufferedImage bgImg;

    private BreakoutDAO dao;

    public PnlGameOver(FrmMain frmMain, Score score, Difficulty difficulty, boolean multiplayer)
    {
        super();

        setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));

        this.frmMain = frmMain;
        this.score = score;
        this.difficulty = difficulty;
        this.multiplayer = multiplayer;
        dao = Launcher.dao;

        createComponents();
        addComponents();
        addActionListeners();
    }

    private void createComponents() {
        lblTitle = new JLabel("Game Over", SwingConstants.CENTER);
        lblPlayer1 = new JLabel("Player 1", SwingConstants.CENTER);
        lblScore1 = new JLabel("Your score:       " + Integer.toString(score.getScore1()), SwingConstants.LEFT);
        lblMultiplier1 = new JLabel("* " + Double.toString(difficulty.getDifficultyMultiplier()) + " (from difficulty)", SwingConstants.CENTER);
        lblEndscore1 = new JLabel(Integer.toString(score.getEndScore1()), SwingConstants.CENTER);
        lblName1 = new JLabel("Enter your name:", SwingConstants.CENTER);
        txtName1 = new JTextField();
        if (multiplayer) {
            lblPlayer2 = new JLabel("Player 2", SwingConstants.CENTER);
            lblScore2 = new JLabel("Your score:       " + Integer.toString(score.getScore2()), SwingConstants.LEFT);
            lblMultiplier2 = new JLabel("* " + Double.toString(difficulty.getDifficultyMultiplier()) + " (from difficulty)", SwingConstants.CENTER);
            lblEndscore2 = new JLabel(Integer.toString(score.getEndScore2()), SwingConstants.CENTER);
            lblName2 = new JLabel("Enter your name:", SwingConstants.CENTER);
            txtName2 = new JTextField();
        }
        lblEmptySpace = new JLabel();
        lblFinalscore = new JLabel("Total Score: " + Integer.toString(score.getEndFinalScore()), SwingConstants.CENTER);
        btnOk = new JButton("OK");

        addComponentLayouts();

        try {
            bgImg = ImageIO.read(new File("images/background.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void addComponents() {
        add(lblTitle);
        add(lblPlayer1);
        add(lblScore1);
        add(lblMultiplier1);
        add(lblEndscore1);
        add(lblName1);
        add(txtName1);
        if (multiplayer) {
            add(lblPlayer2);
            add(lblScore2);
            add(lblMultiplier2);
            add(lblEndscore2);
            add(lblName2);
            add(txtName2);
        }
        add(lblEmptySpace);
        add(lblFinalscore);
        add(btnOk);
    }

    private void addActionListeners() {
        final PnlGameOver that = this;
        btnOk.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(txtName1.getText().length() > 50 || (multiplayer && txtName2.getText().length() > 50)) {
                    JOptionPane.showMessageDialog(that, "Name can not be longer than 50 chars.", "Error", JOptionPane.ERROR_MESSAGE);
                }
                else {
                    if (multiplayer) {
                        insertMP();
                    } else {
                        insertSP();
                    }
                    backToMenu();
                }
            }
        });
    }

    private void addComponentLayouts() {
        addComponentLayout(lblTitle, 1280, 100, 30, Color.YELLOW, Color.BLACK, true);
        addComponentLayout(lblPlayer1, 1280, 50, 20, Color.YELLOW, Color.DARK_GRAY, true);
        addComponentLayout(lblScore1, 300, 50, 20, Color.DARK_GRAY, Color.GRAY, false);
        addComponentLayout(lblMultiplier1, 300, 50, 20,  Color.DARK_GRAY, Color.GRAY, false);
        addComponentLayout(lblEndscore1, 300, 50, 20, Color.DARK_GRAY, Color.GRAY, false);
        addComponentLayout(lblName1, 1280, 50, 20, Color.DARK_GRAY, Color.GRAY, false);
        addComponentLayout(txtName1, 640, 40, 20, Color.BLACK, Color.WHITE, true);
        addComponentLayout(lblEmptySpace, 1280, 255, 20, Color.GRAY, Color.GRAY, false);
        if (multiplayer) {
            addComponentLayout(lblPlayer2, 1280, 50, 20, Color.YELLOW, Color.DARK_GRAY, true);
            addComponentLayout(lblScore2, 300, 50, 20, Color.DARK_GRAY, Color.GRAY, false);
            addComponentLayout(lblMultiplier2, 300, 50, 20, Color.DARK_GRAY, Color.GRAY, false);
            addComponentLayout(lblEndscore2, 300, 50, 20, Color.DARK_GRAY, Color.GRAY, false);
            addComponentLayout(lblName2, 1280, 50, 20, Color.DARK_GRAY, Color.GRAY, false);
            addComponentLayout(txtName2, 640, 40, 20, Color.BLACK, Color.WHITE, true);
            addComponentLayout(lblEmptySpace, 1280, 65, 20, Color.GRAY, Color.GRAY, false);
        }
        addComponentLayout(lblFinalscore, 1280, 75, 25, Color.YELLOW, Color.DARK_GRAY, true);
        addComponentLayout(btnOk, 1280, 100, 30, Color.YELLOW, Color.BLACK, true);

    }

    private void addComponentLayout(JComponent cmp, int width, int height, int fontSize, Color fgColor, Color bgColor, boolean isOpaque) {
        cmp.setOpaque(isOpaque);
        cmp.setFont(new Font("Impact", Font.PLAIN, fontSize));
        cmp.setForeground(fgColor);
        cmp.setBackground(bgColor);
        cmp.setPreferredSize(new Dimension(GameResizer.resizeInt(width), GameResizer.resizeInt(height)));
    }

    private void backToMenu()
    {
        frmMain.getContentPane().removeAll();
        frmMain.setContentPane(new PnlMenu(new FrmMain()));
        frmMain.validate();
        frmMain.repaint();
        frmMain.dispose();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(bgImg, 0,0, frmMain.getWidth(), frmMain.getHeight(), null);
    }

    private void insertSP() {
        if(!txtName1.getText().equals("") && txtName1.getText() != null ){
            PlayerSP player = new PlayerSP(score.getEndFinalScore(), txtName1.getText());
            try {
                dao.insertSPHighscore(player);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
    }

    private void insertMP() {
        if(!txtName1.getText().equals("") && txtName1.getText() != null && !txtName2.getText().equals("") && txtName2.getText() != null){
            PlayersMP players = new PlayersMP(score.getEndScore1(), score.getEndScore2(), score.getEndFinalScore(), txtName1.getText(), txtName2.getText());
            PlayerSP player1 = new PlayerSP(players.getScore1(), players.getName1());
            PlayerSP player2 = new PlayerSP(players.getScore2(), players.getName2());
            try {
                dao.insertMPHighscore(player1, player2);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
    }
}
