package be.howest.groep10.UIL;

import be.howest.groep10.BOL.Game.Difficulty;
import be.howest.groep10.BOL.Game.FrmMain;
import be.howest.groep10.BOL.Game.MPGameBoard;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Siebe on 20/11/2014.
 */
public class PnlMPGame extends JPanel {
    private FrmMain frmMain;
    private PnlMPGameScore pnlMPGameScore;
    private MPGameBoard MPGameBoard;
    private Difficulty diff;

    public PnlMPGame(FrmMain frmMain, Difficulty difficulty) {
        super();

        this.frmMain = frmMain;
        diff = difficulty;
        setPreferredSize(new Dimension(frmMain.getWidth(), frmMain.getHeight()));
        setLayout(new BorderLayout());
        createComponents();
        addComponents();
    }

    private void createComponents() {
        MPGameBoard = new MPGameBoard(frmMain, diff);
        frmMain.addKeyListener(MPGameBoard);
        pnlMPGameScore = new PnlMPGameScore(frmMain, MPGameBoard);
    }

    private void addComponents() {
        add(MPGameBoard, BorderLayout.CENTER);
        add(pnlMPGameScore, BorderLayout.EAST);
    }
}
