package be.howest.groep10.UIL;

import be.howest.groep10.BOL.Game.GameResizer;
import be.howest.groep10.DAL.BreakoutDAO;
import be.howest.groep10.DAL.PlayerSP;
import be.howest.groep10.BOL.Game.Launcher;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by Siebe on 8/11/2014.
 */
public class PnlHSSPCenter extends JPanel {
    private String[] columnTitles = {"Rank", "Name", "Score"};
    private List<PlayerSP> playerList;
    private String[][] scoreList;
    private JTable scoreTable;
    private BreakoutDAO dao;

    private BufferedImage bgImg;

    public PnlHSSPCenter() {
        super();

        createComponents();
        addComponents();
    }

    private void createComponents() {
        try {
            dao = Launcher.dao;
            playerList = dao.getSPHighscoreList();
        } catch (Exception e) {
            e.printStackTrace();
        }

        scoreList = new String[playerList.size()][3];

        for (int i = 0; i < playerList.size(); i++) {
            scoreList[i][0] = i + 1 + ".";
            scoreList[i][1] = playerList.get(i).getName();
            scoreList[i][2] = Integer.toString(playerList.get(i).getScore());
        }

        scoreTable = new JTable(scoreList, columnTitles);

        addComponentLayout(scoreTable.getTableHeader(), 1180, 45, 25, Color.YELLOW, Color.DARK_GRAY);
        addComponentLayout(scoreTable, 1180, 500, 20, Color.DARK_GRAY, Color.GRAY);
        addTableLayout(scoreTable, 80, 550);

        try {
            bgImg = ImageIO.read(new File("images/background.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void addComponents() {
        add(scoreTable.getTableHeader());
        add(scoreTable);
    }

    private void addComponentLayout(JComponent cmp, int width, int height, int fontSize, Color fgColor, Color bgColor) {
        cmp.setFont(new Font("Impact", Font.PLAIN, GameResizer.resizeInt(fontSize)));
        cmp.setPreferredSize(new Dimension(GameResizer.resizeInt(width), GameResizer.resizeInt(height)));
        cmp.setBackground(bgColor);
        cmp.setForeground(fgColor);
    }

    private void addTableLayout(JTable tbl, int columnWidth1, int columnWidth2) {
        TableColumn column = null;
        for (int i = 0; i < 2; i++) {
            column = tbl.getColumnModel().getColumn(i);
            if (i == 0) {
                column.setPreferredWidth(GameResizer.resizeInt(columnWidth1));
            } else {
                column.setPreferredWidth(GameResizer.resizeInt(columnWidth2));
            }
        }
        tbl.setRowHeight(GameResizer.resizeInt(50));
        tbl.setShowGrid(false);
        tbl.setEnabled(false);
        tbl.setBackground(new Color(255, 255, 255, 0));
        tbl.getTableHeader().setEnabled(false);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(bgImg, 0,0, GameResizer.resizeInt(1280), GameResizer.resizeInt(720), null);
    }
}
