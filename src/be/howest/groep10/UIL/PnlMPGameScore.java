package be.howest.groep10.UIL;

import be.howest.groep10.BOL.Game.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by Siebe on 20/11/2014.
 */
public class PnlMPGameScore extends JPanel implements ScoreObserver {
    private MPGameBoard gameBoard;
    private JButton btnMenu;
    private JLabel lblScore, lblScoreP1, lblScoreP2, lblLevel,lblEmptySpace, lblEmptySpace2;
    private FrmMain frmMain;
    private int score1 =0, score2 = 0, totalScore = 0, nrOfLives, level =1, timerSeconds = 10;
    BufferedImage imgLives;
    BufferedImage imgReverseControls;
    private Score model;
    private JLabel lblBlackRect;
    private JLabel lblTmpPowerTimer;

    private int width = GameResizer.resizeInt(280), heigth = GameResizer.resizeInt(720);

    public PnlMPGameScore(FrmMain frmMain, MPGameBoard gameBoard) {
        super();

        this.frmMain = frmMain;

        ((FlowLayout)this.getLayout()).setVgap(0);
        setBackground(Color.GRAY);
        setPreferredSize(new Dimension(width, heigth));
        this.model = gameBoard.getScore();
        this.gameBoard = gameBoard;
        createComponents();
        addComponents();
        addActionListeners();
        this.gameBoard.addObserver(this);

        try
        {
            imgLives = ImageIO.read(new File("images/life.png"));
            imgReverseControls = ImageIO.read(new File("images/reverseControls.png"));
        }
        catch (IOException ie)
        {
            System.out.println("Some image in the SPGameScore class did not load correctly");
        }
    }

    private void createComponents() {
        lblScoreP1 = new JLabel("<P1 score here>", SwingConstants.RIGHT);
        lblScoreP1.setOpaque(true);
        lblEmptySpace = new JLabel();
        lblScore = new JLabel("", SwingConstants.RIGHT);
        lblScore.setOpaque(true);
        lblLevel = new JLabel("Level: " + level);
        lblTmpPowerTimer = new JLabel("");
        lblBlackRect = new JLabel("");
        lblBlackRect.setOpaque(true);
        lblEmptySpace2 = new JLabel();
        lblScoreP2 = new JLabel("<P2 score here>", SwingConstants.RIGHT);
        lblScoreP2.setOpaque(true);
        btnMenu = new JButton("Back to menu");
        addComponentLayout(lblScoreP1, 280, 50, 20, Color.YELLOW, Color.DARK_GRAY);
        addComponentLayout(lblEmptySpace, 280, 185, 20, Color.YELLOW, Color.GRAY);
        addComponentLayout(lblScore, 280, 50, 25, Color.YELLOW, Color.BLACK);
        addComponentLayout(lblLevel, 280, 50, 30, Color.DARK_GRAY, Color.GRAY);
        addComponentLayout(lblTmpPowerTimer, 280, 50, 30 , Color.DARK_GRAY, Color.GRAY);
        addComponentLayout(lblBlackRect, 280, 50, 30, Color.BLACK, Color.BLACK);
        addComponentLayout(lblEmptySpace2, 280, 185, 20, Color.YELLOW, Color.GRAY);
        addComponentLayout(lblScoreP2, 280, 50, 20, Color.YELLOW, Color.DARK_GRAY);
        addComponentLayout(btnMenu, 280, 50, 20, Color.YELLOW, Color.BLACK);
    }

    private void addComponents() {
        add(lblScoreP2);
        add(lblEmptySpace);
        add(lblScore);
        add(lblLevel);
        add(lblTmpPowerTimer);
        add(lblBlackRect);
        add(lblEmptySpace2);
        add(lblScoreP1);
        add(btnMenu);
    }

    private void addActionListeners() {
        btnMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                gameBoard.stopGame();
                switchToPanel(new PnlMenu(new FrmMain()));
                frmMain.dispose();
            }
        });
    }

    private void addComponentLayout(JComponent cmp, int width, int height, int fontSize, Color fgColor, Color bgColor) {
        cmp.setPreferredSize(new Dimension(GameResizer.resizeInt(width), GameResizer.resizeInt(height)));
        cmp.setFont(new Font("Impact", Font.PLAIN, fontSize));
        cmp.setForeground(fgColor);
        cmp.setBackground(bgColor);
    }

    public void switchToPanel(JPanel panel) {
        frmMain.getContentPane().removeAll();
        frmMain.setContentPane(panel);
        frmMain.validate();
        frmMain.repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        int xPos = GameResizer.resizeInt(245);
        for(int i = 0; i < nrOfLives; i++)
        {
            g.drawImage(imgLives,xPos,GameResizer.resizeInt(345),30, 30, null);
            xPos -= GameResizer.resizeInt(35);
        }
        if(timerSeconds > 0)
            g.drawImage(imgReverseControls, GameResizer.resizeInt(40), GameResizer.resizeInt(345), 30,30, null);
    }

    @Override
    public void update() {
        score1 = model.getScore1();
        score2 = model.getScore2();
        totalScore = model.getTotalScore();
        nrOfLives = model.getLivesLeft();
        lblLevel.setText(" Level: " + model.getLevel());
        lblScore.setText(Integer.toString(totalScore));
        lblScoreP1.setText(Integer.toString(score1));
        lblScoreP2.setText(Integer.toString(score2));
        timerSeconds = model.getTmpTimerSeconds();
        if(timerSeconds <= 0)
            lblTmpPowerTimer.setText("");
        else
            lblTmpPowerTimer.setText(" " + Integer.toString(timerSeconds));
        repaint();
    }

    public int getWidth()
    {
        return width;
    }

    public int getHeigth()
    {
        return heigth;
    }
}
