package be.howest.groep10.UIL;

import be.howest.groep10.BOL.Game.Difficulty;
import be.howest.groep10.BOL.Game.FrmMain;
import be.howest.groep10.BOL.Game.SPGameBoard;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Siebe on 6/11/2014.
 */
public class PnlSPGame extends JPanel {
    private FrmMain frmMain;
    private PnlSPGameScore pnlSPGameScore;
    private SPGameBoard SPGameBoard;
    private Difficulty diff;

    public PnlSPGame(FrmMain frmMain, Difficulty difficulty) {
        super();

        this.frmMain = frmMain;
        diff = difficulty;
        setPreferredSize(new Dimension(frmMain.getWidth(), frmMain.getHeight()));
        setLayout(new BorderLayout());
        createComponents();
        addComponents();
    }

    private void createComponents() {
        SPGameBoard = new SPGameBoard(frmMain, diff);
        frmMain.addKeyListener(SPGameBoard);
        pnlSPGameScore = new PnlSPGameScore(frmMain, SPGameBoard);
    }

    private void addComponents() {
        add(SPGameBoard, BorderLayout.CENTER);
        add(pnlSPGameScore, BorderLayout.EAST);
    }
}
