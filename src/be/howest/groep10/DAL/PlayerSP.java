package be.howest.groep10.DAL;

/**
 * Created by Siebe on 4/12/2014.
 */
public class PlayerSP {
    private int score;
    private String name;

    public PlayerSP(int score, String name) {
        setScore(score);
        setName(name);
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
