package be.howest.groep10.DAL;

/**
 * Created by Siebe on 4/12/2014.
 */
public class PlayersMP {
    private int score1;
    private int score2;
    private int totalScore;
    private String name1;
    private String name2;

    public PlayersMP(int score1, int score2, int totalScore, String name1, String name2) {
        setScore1(score1);
        setScore2(score2);
        setTotalScore(totalScore);
        setName1(name1);
        setName2(name2);
    }

    public int getScore1() {
        return score1;
    }

    public void setScore1(int score1) {
        this.score1 = score1;
    }

    public int getScore2() {
        return score2;
    }

    public void setScore2(int score2) {
        this.score2 = score2;
    }

    public int getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(int totalScore) {
        this.totalScore = totalScore;
    }

    public String getName1() {
        return name1;
    }

    public void setName1(String name1) {
        this.name1 = name1;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }
}
