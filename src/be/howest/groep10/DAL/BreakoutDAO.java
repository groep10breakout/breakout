package be.howest.groep10.DAL;

import be.howest.groep10.BOL.Game.GameResizer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Thomas on 4/12/2014.
 */
public class BreakoutDAO {
    private Connection connection;

    public BreakoutDAO() throws Exception {
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        connection = DriverManager.getConnection("jdbc:mysql://localhost/breakoutjdbc?user=root&password=");
    }

    public void insertSPHighscore(PlayerSP player) throws Exception {
        PreparedStatement preparedStatement =
                connection.prepareStatement("INSERT INTO hssp(p1name,p1score) VALUES(?,?)");
        preparedStatement.setString(1, player.getName());
        preparedStatement.setInt(2, player.getScore());
        preparedStatement.executeUpdate();
    }

    public void insertMPHighscore(PlayerSP player1, PlayerSP player2) throws Exception {
        PreparedStatement preparedStatement =
                connection.prepareStatement("INSERT INTO hsmp(p1name,p1score,p2name,p2score,totalScore) VALUES(?,?,?,?,?)");
        preparedStatement.setString(1, player1.getName());
        preparedStatement.setInt(2, player1.getScore());
        preparedStatement.setString(3, player2.getName());
        preparedStatement.setInt(4, player2.getScore());
        preparedStatement.setInt(5, player1.getScore() + player2.getScore());
        preparedStatement.executeUpdate();
    }

    public List<PlayerSP> getSPHighscoreList() throws Exception {
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM hssp ORDER BY p1score DESC LIMIT 10");
        ResultSet rs = preparedStatement.executeQuery();
        List<PlayerSP> highScoreList = new ArrayList<PlayerSP>();

        while (rs != null && rs.next()) {
            PlayerSP player = new PlayerSP(rs.getInt("p1score"), rs.getString("p1name"));
            highScoreList.add(player);
        }
        return highScoreList;
    }

    public List<PlayersMP> getMPHighscoreList() throws  Exception{
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM hsmp ORDER BY totalScore DESC LIMIT 10");
        ResultSet rs = preparedStatement.executeQuery();
        List<PlayersMP> highScoreList = new ArrayList<PlayersMP>();

        while (rs != null && rs.next()) {
            PlayersMP players = new PlayersMP(rs.getInt("p1score"), rs.getInt("p2score"), rs.getInt("totalScore"), rs.getString("p1name"), rs.getString("p2name"));
            highScoreList.add(players);
        }

        return highScoreList;
    }

    public int getInitLives(int diff) throws Exception{
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM difficulties WHERE id = ?");
        preparedStatement.setInt(1, diff);
        ResultSet rs = preparedStatement.executeQuery();

        if (rs != null && rs.next())
            return rs.getInt("lives");
        else
            return 3;
    }

    public int getInitBallSpeed(int diff) throws Exception {
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM difficulties WHERE id = ?");
        preparedStatement.setInt(1, diff);
        ResultSet rs = preparedStatement.executeQuery();

        if (rs != null && rs.next())
            return rs.getInt("bspeed");
        else
            return 3;
    }

    public String getInitPaddleSize(int diff) throws Exception {
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM difficulties WHERE id = ?");
        preparedStatement.setInt(1, diff);
        ResultSet rs = preparedStatement.executeQuery();

        if (rs != null && rs.next())
            return rs.getString("psize");
        else
            return "paddleSizeMinusOne";
    }

    public String getPowerUrl(String name) throws  Exception
    {
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM powers WHERE name =?");
        preparedStatement.setString(1, name);
        ResultSet rs = preparedStatement.executeQuery();
        rs.next();
        String url = rs.getString("url");
        return url;
    }

    public List<String> getPowerList(String type) throws Exception {
        List<String> powerList  = new ArrayList<String>();
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM powers WHERE type =?");
        preparedStatement.setString(1,type);
        ResultSet rs = preparedStatement.executeQuery();
        while(rs != null && rs.next())
        {
            powerList.add(rs.getString("url"));
        }
        return powerList;
    }

    public List<String> getPowerDescriptionList(String type) throws Exception {
        List<String> powerList  = new ArrayList<String>();
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM powers WHERE type =?");
        preparedStatement.setString(1,type);
        ResultSet rs = preparedStatement.executeQuery();
        while(rs != null && rs.next())
        {
            powerList.add(rs.getString("description"));
        }
        return powerList;
    }

    public int getNrOfRows(int level, boolean multiplayer) throws Exception {
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM levels WHERE id = ?");
        preparedStatement.setInt(1, level);
        ResultSet rs = preparedStatement.executeQuery();

        if (rs != null && rs.next()) {
            if(multiplayer)
                return rs.getInt("nrofrowsMP");
            else
                return rs.getInt("nrofrows");
        } else {
            return 3;
        }
    }

    public int getMaxLevel() throws Exception
    {
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT COUNT(*) as 'maxLevel' from levels");
        ResultSet rs = preparedStatement.executeQuery();
        int maxLevel = 1;
        if(rs != null && rs.next())
        {
            maxLevel =  rs.getInt("maxLevel");
        }
        return maxLevel;
    }

    public int getBrickWidth(int level) throws Exception {
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM levels JOIN brickwidth ON levels.bwidthid = brickwidth.id WHERE levels.id = ?");
        preparedStatement.setInt(1, level);
        ResultSet rs = preparedStatement.executeQuery();

        if (rs != null && rs.next()) {
            return GameResizer.resizeInt(rs.getInt("width"));
        } else {
            return GameResizer.resizeInt(80);
        }
    }

    public int getPowerUpFrequency(int level) throws Exception {
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM levels WHERE id = ?");
        preparedStatement.setInt(1, level);
        ResultSet rs = preparedStatement.executeQuery();

        if (rs != null && rs.next()) {
            return rs.getInt("pufreq");
        } else {
            return 7;
        }
    }

    public int getPowerDownFrequency(int level) throws Exception {
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM levels WHERE id = ?");
        preparedStatement.setInt(1, level);
        ResultSet rs = preparedStatement.executeQuery();

        if (rs != null && rs.next()) {
            return rs.getInt("pdfreq");
        } else {
            return 7;
        }
    }

    public int getBrickScore(String color, int width) throws Exception {
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM bricks JOIN brickwidth ON bricks.bwidthid = brickwidth.id WHERE color = ? AND width = ?");
        preparedStatement.setString(1, color);
        preparedStatement.setInt(2, width);
        ResultSet rs = preparedStatement.executeQuery();

        if (rs != null && rs.next()) {
            return rs.getInt("score");
        } else {
            return 60;
        }
    }

    @Override
    public void finalize() throws Throwable {
        super.finalize();
        connection.close();
    }
}
