package be.howest.groep10.DAL;

import be.howest.groep10.BOL.Game.GameResizer;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class BreakoutDAOTest {
    BreakoutDAO dao;

    @Before
    public void setUp() throws Exception {
        dao = new BreakoutDAO();
    }

    @Test
    public void testInsertSPHighscore() throws Exception {
        int firstSize = dao.getSPHighscoreList().size();
        PlayerSP player = new PlayerSP(1000, "Dummy");
        dao.insertSPHighscore(player);
        int newSize = dao.getSPHighscoreList().size();
        if (firstSize < 10) {
            assertSame(newSize, firstSize + 1);
        }
    }

    @Test
    public void testInsertMPHighscore() throws Exception {
        int firstSize = dao.getMPHighscoreList().size();
        dao.insertMPHighscore(new PlayerSP(1000, "dummy"), new PlayerSP(2000, "dommy"));
        int newSize = dao.getMPHighscoreList().size();
        if (firstSize < 10) {
            assertSame(newSize, firstSize + 1);
        }
    }

    @Test
    public void testGetSPHighscoreList() throws Exception {
        List<PlayerSP> list = dao.getSPHighscoreList();

        assertNotNull(list);
        assertNotSame(0, list.size());
    }

    @Test
    public void testGetMPHighscoreList() throws Exception {
        List<PlayersMP> list = dao.getMPHighscoreList();

        assertNotNull(list);
        assertNotSame(0, list.size());
    }

    @Test
    public void testGetInitBallSpeed() throws Exception {
        int bSpeed = dao.getInitBallSpeed(1);
        assertSame(2, bSpeed);

        bSpeed = dao.getInitBallSpeed(2);
        assertSame(3, bSpeed);

        bSpeed = dao.getInitBallSpeed(3);
        assertSame(4, bSpeed);
    }

    @Test
    public void testGetInitPaddleSize() throws Exception {
        String psize = dao.getInitPaddleSize(1);
        assertEquals("normalPaddle", psize);

        psize = dao.getInitPaddleSize(2);
        assertEquals("paddleSizeMinusOne", psize);

        psize = dao.getInitPaddleSize(3);
        assertEquals("paddleSizeMinusOne", psize);
    }

    @Test
    public void testGetInitLives() throws Exception {
        int lives = dao.getInitLives(1);
        assertSame(5, lives);

        lives = dao.getInitLives(2);
        assertSame(3, lives);

        lives = dao.getInitLives(3);
        assertSame(1, lives);
    }

    @Test
    public void testGetPowerUrl() throws Exception {
        String url = dao.getPowerUrl("extraLife");
        assertEquals("images/extraLife", url);
    }

    @Test
    public void testGetPowerList() throws Exception {
        List<String> powerList = dao.getPowerList("up");

        assertNotNull(powerList);
        assertNotSame(0, powerList.size());
    }

    @Test
    public void testGetPowerDescriptionList() throws Exception {
        List<String> powerList = dao.getPowerDescriptionList("up");

        assertNotNull(powerList);
        assertNotSame(0, powerList.size());
    }

    @Test
    public void testgetBrickScore() throws  Exception
    {
        int testScore = dao.getBrickScore("RED", 40);
        assertSame(100, testScore);
    }

    @Test
    public void testgetPowerDownFrequency() throws  Exception
    {
        int testPdFreq = dao.getPowerDownFrequency(2);
        assertSame(15, testPdFreq);
    }

    @Test
    public void testgetPowerUpFrequency() throws  Exception
    {
        int testPuFreq = dao.getPowerUpFrequency(2);
        assertSame(3, testPuFreq);
    }

    @Test
    public void testgetBrickWidth() throws Exception
    {
        int testWidth = dao.getBrickWidth(1);
        assertSame(GameResizer.resizeInt(120), testWidth);
    }

    @Test
    public void getNrOfRows() throws Exception
    {
        int testNrofRowsSP = dao.getNrOfRows(1,false);
        assertSame(2, testNrofRowsSP);

        int testNrofRowsMP = dao.getNrOfRows(1,true);
        assertSame(2, testNrofRowsMP);
    }


}